% Script to perform earth mover's distance (EMD) on 3D histograms
addpath(pwd);

clear all;
% clc;
close all;


% Generate checkerboard pattern and calculate EMD
im1 = zeros(3,4,10);
im2 = im1;
im3 = im1;

im1(2,2,2) = 1;
im2(1,1,1) = 5;
im3(3,3,9) = 10;
% Apply a thresholded ground distance
const_fac = 1;

% XNBP = size(im1,1);
% YNBP = size(im1,2);
% ZNBP = size(im1,3);
column = size(im1,2);
row = size(im1,1);
ZNBP = size(im1,3);
N = column * row * ZNBP;
D = zeros(N,N);
D12 = D;
D11 = D;

% Stretch out histograms
P = (im1(:));
Q = (im2(:));
R = (im3(:));

extra_mass_penalty = -1;
FType = 3;

% Test different threshold values
for ctr = 1:10
    thresh = ctr * const_fac;
    
    tic;
    D11 = GDMtrxForloop_mex(row,column,ZNBP);
    t11 = toc;
    
    i = 1;
    for r1=1:row
        for c1=1:column
            for z1=1:ZNBP               
                j= 1;
                for r2=1:row
                    for c2=1:column
                        for z2=1:ZNBP
                            D12(i,j)= (sqrt((r1-r2)^2 + (c1-c2)^2) + ...
                                min( [abs(z1-z2) ZNBP-abs(z1-z2)] ));
                            j=j+1;
                        end
                    end
                end
                i= i+1;
            end
        end
    end
    %     maxDist = max(D11(:));
    %     %     D2 = min(D2,thresh);
    %     % figure;subplot(1,2,1);imagesc(D11);title('D11');
    %
    %     % --------------- Use coords info to construct dist mtrx --------------
    %     %     tic;
    %     coords = zeros(N,3);
    %     a = 1;
    %     for ctry = 1:YNBP
    %         for ctrx = 1:XNBP
    %             for ctrz = 1:ZNBP
    %                 coords(a,:) = [ctry ctrx ctrz];
    %                 a = a + 1;
    %             end
    %         end
    %     end
    %
    %     D12 = zeros(N,N);
    %     for ctri = 1:N
    %         for ctrj = 1:N
    %             D12(ctri,ctrj) = (sqrt((coords(ctri,1) - coords(ctrj,1))^2 + (coords(ctri,2) - coords(ctrj,2))^2) + ...
    %                 min(abs(coords(ctri,3) - coords(ctrj,3)), ZNBP - abs(coords(ctri,3) - coords(ctrj,3)))) ;
    %         end
    %     end
    %     t12 = toc;
    % --------------- Use coords info to construct dist mtrx --------------
    
    % subplot(1,2,2);imagesc(D12);title('D12');
    %
    %     Xcoords = coords(:,2);
    %     Ycoords = coords(:,1);
    %     Zcoords = coords(:,3);
    %
    %     tic;
    %     D14 = GDMtrxForloop_coords(Xcoords,Ycoords,Zcoords,N);
    %     t14 = toc;
    %
    
    % codegen -o GDMtrxForloop_coords_mex -report GDMtrxForloop_coords.m -args {Xcoords,Ycoords,Zcoords,N}
    
    %     tic;
    %     D13 = GDMtrxForloop_coords_mex(Xcoords,Ycoords,Zcoords,N);
    %     t13 = toc;
    
    %     disp(['D11 isequal D12? ' num2str(isequal(D11,D12))])
    
    % Use original coordinates to construct ground distance matrix
    % Generate new coordinates according to MATLAB extraction order
    a = 1;
    for z = 1:size(im1,3)
        for c = 1:size(im1,2)
            for r = 1:size(im1,1)
                coords_new(a,:) = [r c z];
                a = a + 1;
            end
        end
    end
    D15 = zeros(N,N);
    for ctri = 1:N
        for ctrj = 1:N
            D15(ctri,ctrj) = sqrt((coords_new(ctri,1) - coords_new(ctrj,1))^2 + (coords_new(ctri,2) - coords_new(ctrj,2))^2) + ...
                min(abs(coords_new(ctri,3) - coords_new(ctrj,3)),max(coords_new(:,3)) - abs(coords_new(ctri,3) - coords_new(ctrj,3)));
        end
    end
    
    % Fastest EMD (thresholded ground distance & metric)
    [dist,~] = emd_hat_gd_metric_mex(P,Q,D11,extra_mass_penalty,FType);
    [dist2,~] = emd_hat_gd_metric_mex(P,R,D11,extra_mass_penalty,FType);
    
    [dist3,~] = emd_hat_gd_metric_mex(P,Q,D15,extra_mass_penalty,FType);
    [dist4,~] = emd_hat_gd_metric_mex(P,R,D15,extra_mass_penalty,FType);
    % Rough idea: since too many zero elements exist, is it possible to keep
    % only non-zero elements and the corresponding ground distance matrix? The
    % following code performs this and tries to find out whether same earth
    % mover's distance will be gathered
    % [dist3,~] = emd_hat_gd_metric_mex(P(1:300),Q(1:300),D11(1:300,1:300),extra_mass_penalty,FType);
    
    % If sampling is applied, the ground distance is different!!!!
    % [dist4,~] = emd_hat_gd_metric_mex(P(1:5:end),R(1:5:end),D11(1:5:end,1:5:end),extra_mass_penalty,FType);
    
    disp(['Threshold value = ' num2str(ctr) ', dist = ' num2str(dist)])
    disp(['Threshold value = ' num2str(ctr) ', dist2 = ' num2str(dist2)])
    disp(['Dist3 = ' num2str(dist3)]);
    disp(['Dist4 = ' num2str(dist4)]);
    
end

correff1 = corr(P,Q);
disp(['Correlation of P & Q = ' num2str(correff1)]);
correff2 = corr(P,R);
disp(['Correlation of P & R = ' num2str(correff2)]);

% --------------------------------------------------------------
% ----- Reduce number of for loops
%     [y1_col,x1_col,z1_col] = meshgrid(1:YNBP,1:XNBP,1:ZNBP);
%     y1_col = y1_col(:);
%     x1_col = x1_col(:);
%     z1_col = z1_col(:);
%
%     idxlist = [z1_col y1_col x1_col];
%     i = 1;
%     for ctrout = 1:size(idxlist,1)
%         j = 1;
%         for ctrin = 1:size(idxlist,1)
%             D2(i,j) = sqrt((idxlist(ctrout,1)-idxlist(ctrin,1))^2 + (idxlist(ctrout,2)-idxlist(ctrin,2))^2) + ...
%                 min(abs(idxlist(ctrout,3)-idxlist(ctrin,3)),ZNBP-abs(idxlist(ctrout,3)-idxlist(ctrin,3)));
%             j = j + 1;
%         end
%         i = i + 1;
%     end
%     D2 = min(D2,thresh);
%     disp(isequal(D,D2));
% ---------------------------------

% Generate C code from MATLAB code




