% This script is used to testify the extracted data
% 
% For All_Subjs_cond_sel_act: the accuracy of decoding based on correlation
% matrix is 87.5%
% 
% For All_Subjs_cond_means_VT05: the decoding accuracy is 62.5%
% 
% For All_Subjs_cond_means_VT: NaN exists
% 
% Need to check these two datasets!!
%%
clear all;
clc;
close all;

addpath('~/Documents/MATLAB/Data/haxby2001/Mat_files');
load('All_Subjs_cond_sel_act.mat');

num_subjs = 6;
num_conds = 8;

all_subjs_squareform_sims = zeros(num_subjs,(num_conds^2 - num_conds)/2);

for ctr = 1:num_subjs
    temp = eval(['sub' num2str(ctr) '_sel_act']);
    corr_mtrx = corr(temp');
    all_subjs_squareform_sims(ctr,:) = squareform(corr_mtrx - eye(num_conds));
    clear temp corr_mtrx;
end

%%
clear all;
clc;
close all;

addpath('~/Documents/MATLAB/Data/haxby2001/Mat_files');
load('All_Subjs_cond_means_VT05.mat');

num_subjs = 6;
num_conds = 8;

all_subjs_squareform_sims = zeros(num_subjs,(num_conds^2 - num_conds)/2);

for ctr = 1:num_subjs
    corr_mtrx = corr(squeeze(all_subjs_cond_means(ctr,:,:))');
    all_subjs_squareform_sims(ctr,:) = squareform(corr_mtrx - eye(num_conds));
    clear temp corr_mtrx;
end