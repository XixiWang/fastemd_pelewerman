% Perform EMD on 1D histograms
% Strach out 3D activity maps into 1D histogram
% Update: need to keep zero elements? The decoding accuracy is increased with zeros? 
% Normalization has no impact on correlation-based decoding
% The decoding accuracy is ~0.65?!!

addpath('~/Documents/MATLAB/Code/FastEMD_PeleWerman');
addpath('~/Documents/MATLAB/Data/haxby2001/Mat_files/Activity maps/')
addpath('~/Documents/MATLAB/Toolboxes/spm8/');
addpath('~/Documents/MATLAB/Toolboxes/NIfTI_20140122/');

const_fac = 1;
extra_mass_penalty = -1;
FType = 3;

% ----------------------------------
thresh = 3000;
% ----------------------------------

[conda,condb] = meshgrid(1:8,1:8);
conda = conda(:); condb = condb(:);
a = 1;
for ctr = 1:length(conda)
    if conda(ctr) > condb(ctr)
        cond_uni(a,1) = conda(ctr);
        cond_uni(a,2) = condb(ctr);
        a = a + 1;
    end
end

% Generate the ground distance matrix (ground distance is same for each
% subject
% temp = load_nii(['selected_voxels_vol.nii']);
% tempdata = temp.img;
% tempdata = tempdata(:);
% N = length(tempdata)
%
% D = ones(N,N) .* thresh;
% for ctri =1:N
%     for ctrj = max([1 ctri - thresh + 1]):min([N ctri + thresh - 1])
%         D(ctri,ctrj) = abs(ctri - ctrj);
%     end
% end

Corr_All = zeros(6,size(cond_uni,1));
for subid = 1:6
    
    disp(['Calculating correlation for subject ' num2str(subid)]);
    
    temp = load_nii(['selected_voxels_vol_subj' num2str(subid) '_cond2.nii']);
    tempimg = temp.img(:);
    % Delete zero elements
    tempimg(tempimg == 0 ) = [];
    
%     N = length(tempimg);
%     disp(['Constructing ground distance matrix for subject ' num2str(subid)]);
%     disp([num2str(N) ' voxels selected']);
%     D= ones(N,N) .* thresh;
%     for ctri = 1:N
%         for ctrj = max([1 ctri - thresh + 1]):min([N ctri + thresh - 1])
%             D(ctri,ctrj) = abs(ctri - ctrj);
%         end
%     end
%     
    for ctr = 1:size(cond_uni,1)
        tempP = load_nii(['selected_voxels_vol_subj' num2str(subid) '_cond' num2str(cond_uni(ctr,1)) '.nii']);
        tempQ = load_nii(['selected_voxels_vol_subj' num2str(subid) '_cond' num2str(cond_uni(ctr,2)) '.nii']);
        P = tempP.img(:);
        Q = tempQ.img(:);
        
        % Delete zero elements
%         P(P == 0) = [];
%         Q(Q == 0) = [];
        P = P./max(P);
        Q = Q./max(Q);
        
        P = double(P);
        Q = double(Q);
        
        % Fastest EMD (thresholded ground distance & metric)
        [dist] = corr(P,Q);
        
        Corr_All(subid,ctr) = dist;
        clear tempP tempQ P Q
    end
    
    disp(['Subject' num2str(subid) ' is done, threshold value = '  num2str(thresh)]);
end

FileName = ['Corr_SelVol_1D_' num2str(thresh) '.mat'];
save(fullfile('~/Documents/MATLAB/Results/Results_EMDAndDecoding',FileName),'Corr_All');