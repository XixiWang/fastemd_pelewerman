clear all;
close all;
% Analyze 1D histograms
% Strech out 2D histograms
% Show different results with correlation coefficient values
im1= zeros(2,2);
im2= zeros(2,2);
im3= zeros(2,2);

im1(1,1) = 1;
im2(2,2) = 1;
im3(1,2) = 1;
figure;subplot(1,3,1);imagesc(im1);subplot(1,3,2);imagesc(im2);subplot(1,3,3);imagesc(im3)

P= im1(:);
Q= im2(:);
O= im3(:);
figure;subplot(1,3,1);imagesc(P);subplot(1,3,2);imagesc(Q);subplot(1,3,3);imagesc(O)

N = length(P);

THRESHOLD= 4;
extra_mass_penalty= -1;
flowType= 3;

D= ones(N,N).*THRESHOLD;
for i=1:N
    for j=max([1 i-THRESHOLD+1]):min([N i+THRESHOLD-1])
        D(i,j)= abs(i-j); 
    end
end

emd_hat_gd_metric_mex_val1= emd_hat_gd_metric_mex(P,Q,D,extra_mass_penalty)
emd_hat_gd_metric_mex_val2= emd_hat_gd_metric_mex(P,O,D,extra_mass_penalty)

correff1 = corr(P,Q)
correff2 = corr(P,O)