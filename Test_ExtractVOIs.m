% Extract VOIs from VT selected whole brain data
clear all;
clc;
close all;

addpath('~/Documents/MATLAB/Data/haxby2001/Mat_files');
cd('~/Documents/MATLAB/Data/haxby2001/Mat_files/Activity maps/VoxelsViaVTMasks_AllSub_AllConds/');

for subid = 1:6
     Vmask = spm_vol(['/axelUsers/xwang91/Documents/MATLAB/Data/haxby2001/subj' num2str(subid) '/SPM/VT_mask/wmask4_vt.nii']);
     Vmaskdata = spm_read_vols(Vmask);
     
     Vmaskdata(isnan(Vmaskdata)) = 0;
     
     idx = find(Vmaskdata);
     [i1,i2,i3] = ind2sub(size(Vmaskdata),idx);
     
     i1min = min(i1(:));
     i1max = max(i1(:));
     
     i2min = min(i2(:));
     i2max = max(i2(:));     
     
     i3min = min(i3(:));
     i3max = max(i3(:));
     
     VOIs = cell(1,8);
     
     for ctr = 1:8
         temp = spm_vol(['VTselected_voxels_vol_subj' num2str(subid) '_cond' num2str(ctr) '.nii']);
         tempdata = spm_read_vols(temp);
         tempVOI = tempdata(i1min-1:i1max+1,i2min-1:i2max+1,i3min:i3max);
         
         VOIs{1,ctr} = tempVOI;
     end 
     
     FileName = (['VTselected_voxels_VOI_subj' num2str(subid) '_allcond.mat']);
     save(fullfile('~/Documents/MATLAB/Data/haxby2001/Mat_files/Activity maps/VOIsViaVTMasks_AllSub_AllConds',FileName),'VOIs');
end

