% This code uses deprecated data
clear all;
close all;

const_fac = 1;
extra_mass_penalty = -1;
FType = 3;

addpath('~/Documents/MATLAB/Code/FastEMD_PeleWerman');
addpath('~/Documents/MATLAB/Data/haxby2001/Mat_files/Activity maps/VOIsViaMVPA_VTmasks_AllSub_AllConds');
addpath('~/Documents/MATLAB/Toolboxes/spm8/');
addpath('~/Documents/MATLAB/Toolboxes/NIfTI_20140122/');
addpath('~/Documents/MATLAB/Utilities/');

[conda,condb] = meshgrid(1:8,1:8);
conda = conda(:); condb = condb(:);
a = 1;
for ctr = 1:length(conda)
    if conda(ctr) < condb(ctr)
        cond_uni(a,1) = conda(ctr);
        cond_uni(a,2) = condb(ctr);
        a = a + 1;
    end
end

% parpool;
for subid = 1
    tic;
    disp(['Constructing ground distance matrix for subject ' num2str(subid)]);
    load(['Averaged_fMRI_VTmasksubj' num2str(subid) '.mat']);
    
    subj_coords = eval(['subj' num2str(subid) '_coords']) + 1;
    subj_coords = double(subj_coords);
    
    Xcoords = subj_coords(:,2);
    Ycoords = subj_coords(:,1);
    Zcoords = subj_coords(:,3);
    voxelnum = size(subj_coords,1);
    
    subj_data = eval(['subj' num2str(subid) '_data']);
    
    % Generate gound distance matrix
    D = GDMtrxForloop_coords(Xcoords,Ycoords,Zcoords,voxelnum);
    thresh1 = round(quantile(D(:),0.95) + 1);
    D1 = min(D,thresh1);
    disp(['max Dist = ' num2str(max(D(:))) ', thresh = ' num2str(thresh1)]);
    
    % Calculate EMD and correlation
    disp(['Calculating EMD & Corr for subject ' num2str(subid)]);
    ind_EMD = zeros(1,size(cond_uni,1));
    ind_Corr = ind_EMD;
    
    % Display parallel processing progress
    N = size(cond_uni,1);
    p = ProgressBar(N);
    for ctr = 1:N
        P = subj_data(cond_uni(ctr,1),:);
        Q = subj_data(cond_uni(ctr,2),:);
        
        ind_Corr(1,ctr) = corr(P',Q');
        
        [dist,~] = emd_hat_gd_metric_mex(P',Q',D1,extra_mass_penalty,FType);
        ind_EMD(1,ctr) = dist;
        
        % Write results to .txt file
        fileID = fopen(['Results_MVPA_Sub' num2str(subid) '.txt'],'a');
        fprintf(fileID,'%12s %12s %12s\r\n','idx 1','idx 2','EMD dist');
        fprintf(fileID,'%12d %12d %12.4e\r\n',cond_uni(ctr,1),cond_uni(ctr,2),dist);
        p.progress;
    end
    p.stop;
    disp(['Subject' num2str(subid) ' is done, threshold value = '  num2str(thresh1)]);
    tEla = toc;
    
    FileName = ['EMDMtrx_MVPA_VOI_3D_' num2str(thresh1) '_sub' num2str(subid) '.mat'];
    save(fullfile('~/Documents/MATLAB/Results/Results_EMDAndDecoding',FileName),'ind_EMD','ind_Corr','tEla');

    % sendmail('xixi.wang577@gmail.com', 'EMD Test', ['Subject ' num2str(subid) ' is done.']);
end
delete(gcp);
