% Update: need to keep zero elements? The decoding accuracy is increased with zeros?
% Normalization has no impact on correlation-based decoding
% clear all;
close all;

addpath('~/Documents/MATLAB/Code/FastEMD_PeleWerman');
cd('~/Documents/MATLAB/Data/haxby2001/Mat_files/Activity maps/VoxelsViaVTMasks_AllSub_AllConds')
addpath('~/Documents/MATLAB/Data/haxby2001/Mat_files/Activity maps')
addpath('~/Documents/MATLAB/Toolboxes/spm8/');
addpath('~/Documents/MATLAB/Toolboxes/NIfTI_20140122/');

[conda,condb] = meshgrid(1:8,1:8);
conda = conda(:); condb = condb(:);
a = 1;
for ctr = 1:length(conda)
    if conda(ctr) > condb(ctr)
        cond_uni(a,1) = conda(ctr);
        cond_uni(a,2) = condb(ctr);
        a = a + 1;
    end
end

Corr_All = zeros(6,size(cond_uni,1));
Corr_All2 = zeros(6,size(cond_uni,1));

for subid = 1:6
    
    disp(['Calculating correlation matrix for subject ' num2str(subid)]);
    % --------------------------------------------------
    %     load(['VTselected_voxels_VOI_subj' num2str(subid) '_allcond.mat']);
    load(['subj' num2str(subid) 'ind_VT.mat']);
    
    % --------------------------------------------------
    
    for ctr = 1:size(cond_uni,1)
        P = eval(['subj' num2str(subid) '_data{1,cond_uni(ctr,1)}']);
        Q = eval(['subj' num2str(subid) '_data{1,cond_uni(ctr,2)}']);
        
        %         P = VOIs{1,cond_uni(ctr,1)};
        %         Q = VOIs{1,cond_uni(ctr,2)};
        
        % ---------------------- Examine 3D volumes ---------------------
        P1 = spm_vol(['VTselected_voxels_vol_subj' num2str(subid) '_cond' num2str(cond_uni(ctr,1)) '.nii']);
        Q1 = spm_vol(['VTselected_voxels_vol_subj' num2str(subid) '_cond' num2str(cond_uni(ctr,2)) '.nii']);
        
        P1 = spm_read_vols(P1);
        Q1 = spm_read_vols(Q1);
        % ---------------------------------------------------------------
        
        % Find NaN elements
        P(isnan(P)) = 0;
        Q(isnan(Q)) = 0;
        P = mean(P);
        Q = mean(Q);
        
        P1(isnan(P1)) = 0;
        Q1(isnan(Q1)) = 0;
        % Total number of voxels: ~ 6000
        
        % Calculate mean activity for each condition
        dist = corr(P',Q');        
        dist2 = corr(P1(:),Q1(:));
        
        Corr_All(subid,ctr) = dist;
        Corr_All2(subid,ctr) = dist2;
        
        clear tempP tempQ P Q P1 Q1
    end
    
    disp(['Subject' num2str(subid) ' is done']);
end

% --------------------------------------------------
FileName1 = 'Corr_VTselected_VOI_ind_subj_vector.mat';
FileName2 = 'Corr_VTselected_VOI_ind_subj_3D.mat';

% --------------------------------------------------
% save(fullfile('~/Documents/MATLAB/Results/Results_EMDAndDecoding',FileName1),'Corr_All');
% save(fullfile('~/Documents/MATLAB/Results/Results_EMDAndDecoding',FileName2),'Corr_All2');
