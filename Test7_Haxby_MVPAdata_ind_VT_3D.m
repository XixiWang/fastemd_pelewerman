% Condition 1 to 8 correspond to:
% bottle, cat, chair, face, house, scissors, scrambledpix, shoes
% Calculate activity maps directly
clear all;
close all;

const_fac = 1;
extra_mass_penalty = -1;
FType = 2;

addpath('~/Documents/MATLAB/Code/FastEMD_PeleWerman');
addpath('~/Documents/MATLAB/Data/haxby2001/Mat_files/Activity maps/VOIsViaMVPA_VTmasks_AllSub_AllConds');
addpath('~/Documents/MATLAB/Toolboxes/spm8/');
addpath('~/Documents/MATLAB/Toolboxes/NIfTI_20140122/');
addpath('~/Documents/MATLAB/Utilities/');

[conda,condb] = meshgrid(1:8,1:8);
conda = conda(:); condb = condb(:);
a = 1;
for ctr = 1:length(conda)
    if conda(ctr) < condb(ctr)
        cond_uni(a,1) = conda(ctr);
        cond_uni(a,2) = condb(ctr);
        a = a + 1;
    end
end

% parpool;
EMD_All = zeros(6,28);
EMD_thr_All = EMD_All;
EMD_Euc_thr_All = EMD_All;
EMD_norm_thr_All = EMD_All;
Corr_All = EMD_All;
Corr_norm_All = EMD_All;

for subid = 1:6
    tic;
    disp(['Constructing ground distance matrix for subject ' num2str(subid)]);
    load(['Averaged_fMRI_VTmasksubj' num2str(subid) '_test2.mat']);
    
    subj_coords = eval(['subj' num2str(subid) '_coords']) + 1;
    subj_coords = double(subj_coords);
    
    % subj_coords: [i, j, k]
    Xcoords = subj_coords(:,2);
    Ycoords = subj_coords(:,1);
    Zcoords = subj_coords(:,3);
    voxelnum = size(subj_coords,1);
    
    subj_data = eval(['subj' num2str(subid) '_data']);
    
    % Generate gound distance matrix
    D = GDMtrxForloop_coords(Xcoords,Ycoords,Zcoords,voxelnum);
    thresh = round(quantile(D(:),0.95) + 1);
    D_thr = min(D,thresh);
    disp(['max Dist = ' num2str(max(D(:))) ', thresh = ' num2str(thresh)]);
    
    % Generate euclidean distance matrix
    D_Euc = zeros(voxelnum,voxelnum);
    for ctri = 1:voxelnum
        for ctrj = 1:voxelnum
            D_Euc(ctri,ctrj) = sqrt((subj_coords(ctri,1) - subj_coords(ctrj,1))^2 + ...
                (subj_coords(ctri,2) - subj_coords(ctrj,2))^2 + (subj_coords(ctri,3) - subj_coords(ctrj,3))^2);
        end
    end
    thresh1 = round(quantile(D_Euc(:),0.95) + 1);
    D_Euc_thr = min(D_Euc,thresh1);
    
    % Generate ground distance matrix
    D2 = zeros(voxelnum,voxelnum);
    for ctri = 1:voxelnum
        for ctrj = 1:voxelnum
            D2(ctri,ctrj) = sqrt((subj_coords(ctri,1) - subj_coords(ctrj,1))^2 + (subj_coords(ctri,2) - subj_coords(ctrj,2))^2) + ...
                min(abs(subj_coords(ctri,3) - subj_coords(ctrj,3)),max(subj_coords(:,3)) - abs(subj_coords(ctri,3) - subj_coords(ctrj,3)));
        end
    end
    thresh2 = round(quantile(D2(:),0.95) + 1);
    D2_thr = min(D2,thresh2);
    
    % Calculate EMD and correlation
    disp(['Calculating EMD & Corr for subject ' num2str(subid)]);
    ind_EMD = zeros(1,size(cond_uni,1));
    ind_EMD_thr = ind_EMD;
    ind_EMD_Euc_thr = ind_EMD;
    ind_EMD_norm_thr = ind_EMD;
    ind_Corr = ind_EMD;
    ind_Corr_norm = ind_EMD;
    
    cond1 = mean(eval(['subj' num2str(subid) '_bottle']));
    cond2 = mean(eval(['subj' num2str(subid) '_cat']));
    cond3 = mean(eval(['subj' num2str(subid) '_chair']));
    cond4 = mean(eval(['subj' num2str(subid) '_face']));
    cond5 = mean(eval(['subj' num2str(subid) '_house']));
    cond6 = mean(eval(['subj' num2str(subid) '_scissors']));
    cond7 = mean(eval(['subj' num2str(subid) '_scrambledpix']));
    cond8 = mean(eval(['subj' num2str(subid) '_shoe']));
    
    subj_cond_all = [cond1;cond2;cond3;cond4;cond5;cond6;cond7;cond8];
    
    % Display parallel processing progress
    N = size(cond_uni,1);
    p = ProgressBar(N);
    for ctr = 1:N
    % parfor ctr = 1:N
        
        P = subj_cond_all(cond_uni(ctr,1),:);
        Q = subj_cond_all(cond_uni(ctr,2),:);
        
        % Normalize activity maps/histograms
        P_norm = (P - min(P(:)))./(max(P(:)) - min(P(:)));
        Q_norm = (Q - min(Q(:)))./(max(Q(:)) - min(Q(:)));
        
        % Calculate correlation
        ind_Corr(1,ctr) = corr(P',Q');
        ind_Corr_norm(1,ctr) = corr(P_norm',Q_norm');
        
        % Fast EMD ground distance
        [dist,~] = emd_hat_gd_metric_mex(P',Q',D,extra_mass_penalty,FType);
        dist_thr = emd_hat_gd_metric_mex(P',Q',D_thr,extra_mass_penalty);
        
        % Euclidean ground distance
        dist_Euc_thr = emd_hat_gd_metric_mex(P',Q',D_Euc_thr,extra_mass_penalty);
        
        % Normalized histograms
        dist_norm_thr = emd_hat_gd_metric_mex(P_norm',Q_norm',D_thr,extra_mass_penalty);
        
        ind_EMD(1,ctr) = dist;
        ind_EMD_thr(1,ctr) = dist_thr;
        ind_EMD_Euc_thr(1,ctr) = dist_Euc_thr;
        ind_EMD_norm_thr(1,ctr) = dist_norm_thr;
        
        % Write results to .txt file
        % fileID = fopen(['Results_MVPA_Sub' num2str(subid) '_indCond.txt'],'a');
        % fprintf(fileID,'%12s %12s %12s\r\n','idx 1','idx 2','EMD dist');
        % fprintf(fileID,'%12d %12d %12.4e\r\n',cond_uni(ctr,1),cond_uni(ctr,2),dist);
        p.progress;
    end
    p.stop;
    disp(['Subject' num2str(subid) ' is done, threshold value = '  num2str(thresh)]);
    tEla = toc;
    
    EMD_All(subid,:) = ind_EMD;
    EMD_thr_All(subid,:) = ind_EMD_thr;
    EMD_Euc_thr_All(subid,:) = ind_EMD_Euc_thr;
    EMD_norm_thr_All(subid,:) = ind_EMD_norm_thr;
    Corr_All(subid,:) = ind_Corr;
    Corr_norm_All(subid,:) = ind_Corr_norm;
    % sendmail('xixi.wang577@gmail.com', 'EMD Test', ['Subject ' num2str(subid) ' is done.']);
end
delete(gcp);

FileName = ['EMDMtrx_MVPA_VOI_3D_norm_All.mat'];
save(fullfile('~/Documents/MATLAB/Results/Results_EMDAndDecoding',FileName), ...
    'EMD_All','EMD_thr_All','EMD_Euc_thr_All','EMD_norm_thr_All','Corr_All','Corr_norm_All');

% %% Test whether the order of orignal histogram matters
% P1 = zeros(size(P));
% Q1 = zeros(size(Q));
%
% P1(1:300) = P(278:577);
% P1(301:end) = P(1:277);
%
% Q1(1:300) = Q(278:577);
% Q1(301:end) = Q(1:277);
%
% subj_coords2 = zeros(size(subj_coords));
% subj_coords2(1:300,:) = subj_coords(278:577,:);
% subj_coords2(301:end,:) = subj_coords(1:277,:);
%
% % Generate ground distance matrix
% D22 = zeros(voxelnum,voxelnum);
% for ctri = 1:voxelnum
%     for ctrj = 1:voxelnum
%         D22(ctri,ctrj) = sqrt((subj_coords2(ctri,1) - subj_coords2(ctrj,1))^2 + (subj_coords2(ctri,2) - subj_coords2(ctrj,2))^2) + ...
%             min(abs(subj_coords2(ctri,3) - subj_coords2(ctrj,3)),max(subj_coords2(:,3)) - abs(subj_coords2(ctri,3) - subj_coords2(ctrj,3)));
%     end
% end
%
% [dist22,~] = emd_hat_gd_metric_mex(P1',Q1',D22,extra_mass_penalty,FType);
