clear all;
close all;

const_fac = 1;
extra_mass_penalty = -1;
FType = 3;

addpath('~/Documents/MATLAB/Code/FastEMD_PeleWerman');
addpath('~/Documents/MATLAB/Data/haxby2001/Mat_files/Activity maps')
addpath('~/Documents/MATLAB/Toolboxes/spm8/');
addpath('~/Documents/MATLAB/Toolboxes/NIfTI_20140122/');

[conda,condb] = meshgrid(1:8,1:8);
conda = conda(:); condb = condb(:);
a = 1;
for ctr = 1:length(conda)
    if conda(ctr) > condb(ctr)
        cond_uni(a,1) = conda(ctr);
        cond_uni(a,2) = condb(ctr);
        a = a + 1;
    end
end

% parpool(6);
for subid = 4
    tic;
    
    disp(['Constructing ground distance matrix for subject ' num2str(subid)]);
    load(['subj' num2str(subid) 'ind_VT.mat']);
    
    subj_coords = eval(['subj' num2str(subid) '_voxel_coords']) + 1;
    subj_coords = double(subj_coords);
    
    N = size(subj_coords,1);
    Xcoords = subj_coords(:,1);
    Ycoords = subj_coords(:,2);
    Zcoords = subj_coords(:,3);
    
    % Generate ground distance matrix
    %     codegen -o GDMtrxForloop_coords_mex -report GDMtrxForloop_coords.m -args {Xcoords,Ycoords,Zcoords,N}
    %     D = GDMtrxForloop_coords_mex(Xcoords,Ycoords,Zcoords,N);
    D = GDMtrxForloop_coords(Xcoords,Ycoords,Zcoords,N);
    %     figure;imagesc(D);
    
    thresh1 = round(quantile(D(:),0.95) + 1);
    D1 = min(D,thresh1);
    disp(['max Dist = ' num2str(max(D(:))) ', thresh = ' num2str(thresh1)]);
    %     figure;imagesc(D1);
    
    %     thresh1 = round(max(D(:)) * 0.75);
    %     D1 = min(D,thresh1);
    %     thresh2 = round(max(D(:)) * 0.5);
    %     D2 = min(D,thresh2);
    
    disp(['Calculating EMD for subject ' num2str(subid)]);
    ind_EMD = zeros(1,size(cond_uni,1));
    
    for ctr = 1:size(cond_uni,1)
        tempP = eval(['subj' num2str(subid) '_data{1,cond_uni(ctr,1)}']);
        tempQ = eval(['subj' num2str(subid) '_data{1,cond_uni(ctr,2)}']);
        
        tempP(isnan(tempP)) = 0;
        tempQ(isnan(tempQ)) = 0;
        
        P = mean(tempP);
        Q = mean(tempQ);
        
        % Fastest EMD (thresholded ground distance & metric)
        [dist,~] = emd_hat_gd_metric_mex(P',Q',D1,extra_mass_penalty,FType);
        
        ind_EMD(1,ctr) = dist;
    end
    
    disp(['Subject' num2str(subid) ' is done, threshold value = '  num2str(thresh1)]);
    tEla = toc;
    
    FileName = ['EMDMtrx_ind_VT_VOI_3D_' num2str(thresh1) '_sub' num2str(subid) '_newGD.mat'];
    save(fullfile('~/Documents/MATLAB/Results/Results_EMDAndDecoding',FileName),'ind_EMD','tEla');
    
    sendmail('xixi.wang577@gmail.com', 'EMD Test', ['Subject ' num2str(subid) ' is done.']);
end
% delete(gcp);
