clear all;
close all;

addpath('~/Documents/MATLAB/Data/haxby2001/Mat_files/Activity maps');

const_fac = 1;
extra_mass_penalty = -1;
FType = 3;
% --------------------------------------
thresh = 2;
% --------------------------------------

addpath('~/Documents/MATLAB/Code/FastEMD_PeleWerman');
cd('~/Documents/MATLAB/Data/haxby2001/Mat_files/Activity maps/VoxelsViaVTMasks_AllSub_AllConds/')
addpath('~/Documents/MATLAB/Toolboxes/spm8/');
addpath('~/Documents/MATLAB/Toolboxes/NIfTI_20140122/');

[conda,condb] = meshgrid(1:8,1:8);
conda = conda(:); condb = condb(:);
a = 1;
for ctr = 1:length(conda)
    if conda(ctr) > condb(ctr)
        cond_uni(a,1) = conda(ctr);
        cond_uni(a,2) = condb(ctr);
        a = a + 1;
    end
end

% Construct ground distance matrix
disp('Constructing ground distance matrix');

tempV = spm_vol('VTselected_voxels_vol_subj1_cond1.nii');
XNBP = tempV.dim(1);
YNBP = tempV.dim(2);
ZNBP = tempV.dim(3);

% D = GDMtrxForloop_mex(YNBP,XNBP,ZNBP);
% D1 = min(D,thresh);

for subid = 1:6

    disp(['Calculating EMD for subject ' num2str(subid)]);
    disp('Analyzing 3D VOIs for each condition')
    
    ind_EMD = zeros(1,size(cond_uni,1));
    
%         matlabpool open;
    for ctr = 1:size(cond_uni,1)
        tempP = spm_vol(['VTselected_voxels_vol_subj' num2str(subid) '_cond' num2str(cond_uni(ctr,1)) '.nii']);
        tempQ = spm_vol(['VTselected_voxels_vol_subj' num2str(subid) '_cond' num2str(cond_uni(ctr,2)) '.nii']);
                
        tempP = spm_read_vols(tempP);
        tempQ = spm_read_vols(tempQ);
        
        % Delete NaN elements
        tempP(isnan(tempP)) = 0;
        tempQ(isnan(tempQ)) = 0;
        
        P = tempP(:);
        Q = tempQ(:);
        
        % Fastest EMD (thresholded ground distance & metric)
        [dist,~] = emd_hat_gd_metric_mex(P',Q',D1,extra_mass_penalty,FType);
        
        ind_EMD(1,ctr) = dist;
    end
%         matlabpool close;
    
    disp(['Subject' num2str(subid) ' is done, threshold value = '  num2str(thresh)]);
    
    FileName = ['EMDMtrx_ind_VT_3D_' num2str(thresh) '_sub' num2str(subid) '.mat'];
    save(FileName,'ind_EMD');
end