close all; clear all;
% Analyze 2D histograms
% Check the definition of ground distance matrix and final results later!!!
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% load images
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
im1= zeros(10,10);
im2= im1;
im3= im1;

im1(1,1) = 1;
im2(2,1) = 1;
im3(1,2) = 1;

im1 = double(im1);
im2 = double(im2);
im3 = double(im3);

R= size(im1,1);
C= size(im1,2);
figure;subplot(1,3,1);imagesc(im1);subplot(1,3,2);imagesc(im2);subplot(1,3,3);imagesc(im3)
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% EMD input
% Each unit of gray-level (between 0 and 255) is a unit of mass, and the
% ground distance is a thresholded distance. This is similar to:
%  A Unified Approach to the Change of Resolution: Space and Gray-Level
%  S. Peleg and M. Werman and H. Rom
%  PAMI 11, 739-742
% The difference is that the images do not have the same total mass 
% and we chose a thresholded ground distance
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
COST_MULT_FACTOR= 1000;
THRESHOLD= 2*COST_MULT_FACTOR;
D= zeros(R*C,R*C);
j= 0;
for c1=1:C
    for r1=1:R
        j= j+1;
        i= 0;
        for c2=1:C
            for r2=1:R
                i= i+1;
                D(i,j)= min( [THRESHOLD (COST_MULT_FACTOR*sqrt((r1-r2)^2+(c1-c2)^2))] );
            end
        end
    end
end

D1= zeros(R*C,R*C);
j= 0;
for c1=1:C
    for r1=1:R
        j= j+1;
        i= 0;
        for c2=1:C
            for r2=1:R
                i= i+1;
                D1(i,j)= COST_MULT_FACTOR*sqrt((r1-r2)^2+(c1-c2)^2);
            end
        end
    end
end
extra_mass_penalty= double(-1);
flowType= double(3);

P= double(im1(:));
Q= double(im2(:));
O= double(im3(:));
figure;subplot(1,3,1);imagesc(P);subplot(1,3,2);imagesc(Q);subplot(1,3,3);imagesc(O)
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% The demo includes several ways to call emd_hat_mex and emd_hat_gd_metric_mex
emd_hat_gd_metric_mex_val1= emd_hat_gd_metric_mex(P,Q,D,extra_mass_penalty)
emd_hat_gd_metric_mex_val2= emd_hat_gd_metric_mex(P,O,D,extra_mass_penalty)

emd_hat_gd_metric_mex_val3= emd_hat_gd_metric_mex(P,Q,D1,extra_mass_penalty)
emd_hat_gd_metric_mex_val4= emd_hat_gd_metric_mex(P,O,D1,extra_mass_penalty)
