% Perform earth mover's distance (EMD) on 1D histograms
% Voxels selected based on whole brain mask & feature selection (t-maps and
% F-maps: All_Subjs_cond_sel_act.mat
% 
% 
addpath(pwd);
addpath('~/Documents/MATLAB/Data/haxby2001/Mat_files/Activity maps');
% cd('~/Documents/MATLAB/Data/haxby2001/Mat_files/Activity maps');

clear all;
% clc;
close all;

% Voxels selected via feature selection
% Selected based on t values and F values
load('All_Subjs_cond_sel_act.mat');

const_fac = 1;
extra_mass_penalty = -1;
FType = 3;

[conda,condb] = meshgrid(1:8,1:8);
conda = conda(:); condb = condb(:);
a = 1;
for ctr = 1:length(conda)
    if conda(ctr) > condb(ctr)
        cond_uni(a,1) = conda(ctr);
        cond_uni(a,2) = condb(ctr);
        a = a + 1;
    end
end

EMDMtrx_Sel = zeros(6,size(cond_uni,1));

for subid = 1:6
    Subj_fMRI_value = eval(['sub' num2str(subid) '_sel_act']);
    ncond = size(Subj_fMRI_value,1);
    
    DistMtrx2 = zeros(1,size(cond_uni,1));
    
    for condctr = 1:size(cond_uni,1)
        P = (Subj_fMRI_value(cond_uni(condctr,1),:))';
        Q = (Subj_fMRI_value(cond_uni(condctr,2),:))';
        
        % Apply a thresholded ground distance
        % Test different threshold values
        % ---------------------------------------
        for ctr = 7
        % ---------------------------------------
            thr = ctr * const_fac;
            
            % Generate ground distance mtrx
            D = ones(size(Subj_fMRI_value,2),size(Subj_fMRI_value,2));
            for ctri = 1:size(Subj_fMRI_value,2)
                for ctrj = max(1,ctri - thr + 1) : min(size(Subj_fMRI_value,2),ctri + thr - 1)
                    D(ctri,ctrj) = abs(ctri - ctrj);
                end
            end
            
            % Fastest EMD (thresholded ground distance & metric)
            [dist,~] = emd_hat_gd_metric_mex(P,Q,D,extra_mass_penalty,FType);
            % disp(['Threshold value = ' num2str(ctr) ', dist = ' num2str(dist)])
            
            DistMtrx2(1,condctr) = dist;
            % Thresholded ground distance only
            % [dist2,~]= emd_hat_mex(P,Q,D,extra_mass_penalty,FType);
            % disp(dist2)
            
            % Rubner EMD
            % sumBig=   max([sum(P(:)) sum(Q(:))]);
            % sumSmall= min([sum(P(:)) sum(Q(:))]);
            % D= double(D);
            % emd_rubner_mex_val= (sumSmall*emd_mex(P',Q',D)) + (sumBig-sumSmall)*max(D(:));
        end
    end
    
    disp(['Subject' num2str(subid) 'is done.'])
    EMDMtrx_Sel(subid,:) = DistMtrx2;
    clear DistMtrx2;
end

FileName = ['EMDMtrx_sel_act_thr_' num2str(thr) '.mat'];
save(fullfile('~/Documents/MATLAB/Results/Results_EMDAndDecoding',FileName),'EMDMtrx_Sel');

