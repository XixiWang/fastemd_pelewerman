clear all;
% clc;
close all;

addpath('~/Documents/MATLAB/Results/Results_EMDAndDecoding/');

% -------------------
FileName = 'EMDMtrx_MVPA_VOI_3D_All.mat';
load(FileName)
DisMtrx = EMD_All;
% Normalization types: 'ind' (individual) and 'glo' (global)
normtype = 'ind';
% -------------------

switch normtype
    case 'ind'
        % Normalize EMD for each subject separately
        DisMtrx_pro = zeros(6,28);
        for ctr = 1:6
            DisMtrx_pro(ctr,:) = (DisMtrx(ctr,:) - min(DisMtrx(ctr,:)))./(max(DisMtrx(ctr,:)) - min(DisMtrx(ctr,:)));
        end
    case 'glo'
        % Global normalize the EMD matrix
        DisMtrx_pro = (DisMtrx - min(DisMtrx(:)))./(max(DisMtrx(:)) - min(DisMtrx(:)));
end

% Convert to similarity values
SimMtrx = 1 - DisMtrx_pro;

% Use similarity matrix for neural decoding
Sim_true = SimMtrx';
labels = {'bottle','cat','chair','face','house','scissors','scrambledpix','shoe'};

num_subjs = 6;
num_conds = 8;
all_perms = perms(1:num_conds);
num_perms = factorial(num_conds);

length_square_vec = size(DisMtrx,2);
squareform_extractor_mat = tril(true(num_conds),-1);

% ----------
% Calculation type: 'zscore' and 'ori' (original)
CalType = 'zscore';
% ----------

for subj_num = 1:num_subjs
    ind_sim_true = Sim_true(:,subj_num);
    ind_sim_true_mat = squareform(ind_sim_true);
    
    other_subjs = setdiff(1:num_subjs,subj_num);
    other_sim_true = Sim_true(:,other_subjs);
    other_sim_true_vec = mean(other_sim_true,2);
    
    switch CalType
        case 'zscore'
            % For selected subject
            ind_sim_true_mean = mean(ind_sim_true);
            ind_sim_true_std = std(ind_sim_true);
            ind_sim_true_zscore = (ind_sim_true - ind_sim_true_mean)/ind_sim_true_std;
            ind_sim_true_fin = squareform(ind_sim_true_zscore);
            
            % For all other subjects
            other_sim_true_mean = mean(other_sim_true_vec);
            other_sim_true_std = std(other_sim_true_vec);
            other_sim_true_zscore = (other_sim_true_vec - other_sim_true_mean)/other_sim_true_std;
            other_sim_true_fin = squareform(other_sim_true_zscore);
            
        case 'ori'
            ind_sim_true_fin = ind_sim_true_mat;
            other_sim_true_fin = squareform(other_sim_true_vec);
    end
    
    % Check all permutations
    corr_all_perms = zeros(1,num_perms);
    for ctr = 1:num_perms
        ind_perm = all_perms(ctr,:);
        
        % Generate permed-label similarity matrix
        sim_mat_ind_perm = ind_sim_true_fin(ind_perm,ind_perm);
        sim_vec_ind_perm = sim_mat_ind_perm(squareform_extractor_mat);
        this_perm_corr = corr(sim_vec_ind_perm,other_sim_true_vec);
        
        corr_all_perms(ctr) = this_perm_corr;
    end
    
    [maxcorr,maxcorridx] = max(corr_all_perms);
    
    ind_best_perm = all_perms(maxcorridx,:);
    disp(['Subj ' num2str(subj_num) ': Best-matching perm with other subjects was ' ...
        num2str(ind_best_perm) ])
   end