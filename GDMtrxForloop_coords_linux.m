function D = GDMtrxForloop_coords_linux(Xcoords,Ycoords,Zcoords,N) %#codegen
assert(isa(Ycoords,'double'));
assert(isa(Xcoords,'double'));
assert(isa(Zcoords,'double'));
assert(isa(N,'double'));

ZNBP0 = max(Zcoords(:));
D = zeros(N,N);
    for ctri = 1:N
        for ctrj = 1:N
            D(ctri,ctrj) = (sqrt((Ycoords(ctri) - Ycoords(ctrj))^2 + (Xcoords(ctri) - Xcoords(ctrj))^2) + ...
                min(abs(Zcoords(ctri) - Zcoords(ctrj)), ZNBP0 - abs(Zcoords(ctri) - Zcoords(ctrj)))) ;
        end
    end
end