% Script to perform earth mover's distance (EMD) on 2D histograms
addpath(pwd);

clear all;
% clc;
close all;

% Create checkerboard pattern
% im1 = double(imread('cameraman.tif'));
% im2 = double(imread('rice.png'));
%
% Resize images
% im1 = imresize(im1,1/4);
% im2 = imresize(im2,1/4);

% Generate checkerboard pattern and calculate EMD
im1 = zeros(15,25);
im2 = im1;
% im2 = zeros(32,32);
% im1(1:3:end,:) = 1;
% im2(2:2:end,1:3:end) = 1;
% figure;imshow(im1);figure;imshow(im2);

% Check size of images
if (~(size(im2,1) == size(im1,1) && size(im2,2) == size(im1,2)))
    error('Size of images should be the same');
end

% Apply a thresholded ground distance
const_fac = 1000;

% Stretch out histograms
P = (im1(:));
Q = (im2(:));

extra_mass_penalty = -1;
FType = 3;

% Test different threshold values
for ctr = 1:10
    Threshold = ctr * const_fac;
    
    % Initialize the ground distance mtrx
    [R,C] = size(im1);
    [tempR,tempC] = meshgrid(1:R,1:C);
    tempR_col = tempR(:); tempC_col = tempC(:);
    
    % D = zeros(R*C,R*C,'int32');
    D = zeros(R*C,R*C);
    j = 0;
    for ctri = 1:length(tempR_col)
        j = j + 1;
        i = 0;
        for ctrj = 1:length(tempR_col)
            i = i + 1;
            D(i,j) = min(Threshold, const_fac * ...
                sqrt((tempR_col(ctri) - tempR_col(ctrj))^2 + (tempC_col(ctri) - tempC_col(ctrj))^2));
        end
    end
    
    D1= zeros(R*C,R*C);
    j= 0;
    for c1=1:C
        for r1=1:R
            j= j+1;
            i= 0;
            for c2=1:C
                for r2=1:R
                    i= i+1;
                    D1(i,j)= min( [Threshold (const_fac*sqrt((r1-r2)^2+(c1-c2)^2))] );
                end
            end
        end
    end
    
    % Fastest EMD (thresholded ground distance & metric)
    [dist,~] = emd_hat_gd_metric_mex(P,Q,D,extra_mass_penalty,FType);
    disp(['Threshold value = ' num2str(ctr) ', dist = ' num2str(dist)])
    
    % Thresholded ground distance only
    % [dist2,~]= emd_hat_mex(P,Q,D,extra_mass_penalty,FType);
    % disp(dist2)
    
    % Rubner EMD
    % sumBig=   max([sum(P(:)) sum(Q(:))]);
    % sumSmall= min([sum(P(:)) sum(Q(:))]);
    % D= double(D);
    % emd_rubner_mex_val= (sumSmall*emd_mex(P',Q',D)) + (sumBig-sumSmall)*max(D(:));
end


