% Perform EMD on 2D histogram

clear all;
% clc;
close all;

addpath('/axelUsers/xwang91/Documents/MATLAB/Code/FastEMD_PeleWerman/Data_Haxby_ActMaps');
addpath('/axelUsers/xwang91/Documents/MATLAB/Code/piotr_toolbox/toolbox/classify');

load('All_subs_cond_sel_act.mat');

const_fac = 1;
extra_mass_penalty = -1;
FType = 3;

[conda,condb] = meshgrid(1:8,1:8);
conda = conda(:); condb = condb(:);
a = 1;
for ctr = 1:length(conda)
    if conda(ctr) > condb(ctr)
        cond_uni(a,1) = conda(ctr);
        cond_uni(a,2) = condb(ctr);
        a = a + 1;
    end
end

EMDMtrx_Sel = zeros(6,size(cond_uni,1));

for subid = 1:6
    Averaged_fMRI_values_sel = eval(['sub' num2str(subid) '_sel_act']);
    ncond = size(Averaged_fMRI_values_sel,1);
    
    DistMtrx2 = zeros(1,size(cond_uni,1));
    
    for condctr = 1:size(cond_uni,1)
        P = Averaged_fMRI_values_sel(cond_uni(condctr,1),:);
        Q = Averaged_fMRI_values_sel(cond_uni(condctr,2),:);
        D = pdist3(P,Q,'emd');
        
        DistMtrx2(1,condctr) = D;
    end
    
    disp(['Subject' num2str(subid) 'is done.'])
    EMDMtrx_Sel(subid,:) = DistMtrx2;
    clear DistMtrx2;
end

save(fullfile(pwd,'Results_EMDAndDecoding','EMDMtrx_SelVol_pdist.mat'),'EMDMtrx_Sel');