% Perform across subject decoding based on EMD distance matrices
clear all;
clc;
close all;

cd('~/Documents/MATLAB/Results/Results_EMDAndDecoding/');

load('EMDMtrx_MVPA_VOI_3D_All.mat');

% Global normalize the EMD mtrx;
EMD_All_norm = (EMD_All - min(EMD_All(:)))./(max(EMD_All(:)) - min(EMD_All(:)));

% Normalize each subject separately
% EMD_All_norm = zeros(6,28);
% for ctr = 1:6
%     EMD_All_norm(ctr,:) = (EMD_All(ctr,:) - min(EMD_All(ctr,:)))./(max(EMD_All(ctr,:)) - min(EMD_All(ctr,:)));
% end

% Normalize EMD to [0,2]
% EMD_All = zeros(6,28);
% for ctr = 1:6
%     EMD_All(ctr,:) = ((EMDMtrx_All(ctr,:) - min(EMDMtrx_All(ctr,:)))./(max(EMDMtrx_All(ctr,:)) - min(EMDMtrx_All(ctr,:))))*2;
% end

% Assume taking the global maximum value?
Corr_All = 1 - EMD_All_norm;

% Apply corr_all matrix for neural decoding
all_subjs_squareform_sims = Corr_All;
the_sims_made_from_true_labels = all_subjs_squareform_sims';
labels = {'bottle ' 'cat ' 'chair ' 'face ' 'house ' 'scissors ' 'scrambledpix ' 'shoe '};

num_subjs = size(the_sims_made_from_true_labels,2);
num_conds = length(labels);

all_possible_perms = perms(1:num_conds);  %%% For 8 conds, there are 40320 = 8!
num_perms = factorial(num_conds);

length_of_a_squareform_vec = num_conds*(num_conds-1)/2;
% Initialize the similarity matrix
squareform_extractor_mat = tril(true(num_conds),-1);

best_matching_perms_rec = zeros(num_conds,num_subjs);
subjects_prop_correct_rec = zeros(1,num_subjs);
classes_correct_rec = zeros(num_subjs,num_conds);

Results = cell(8,4);
Results{1,1} = 'SubID';
Results{1,2} = 'Best-matching perm';
Results{1,3} = 'r';
Results{1,4} = 'Prop correct';

for subj_num = 1:num_subjs,
    
    %%% Read in the column of true sims for this subj
    this_subj_true_sims = the_sims_made_from_true_labels(:,subj_num);
    %    For each subject, generate the similarity matirx
    this_subj_true_sim_matrix = squareform(this_subj_true_sims);
    
    this_subj_true_sim_mat_zscored = this_subj_true_sim_matrix;
    
    %%% Now let's see which of these perms matches best with the average
    %%% sim-matrix from the other subjects
    the_other_subjects = setdiff([1:num_subjs],subj_num);
    sim_vecs_from_the_other_subjects = the_sims_made_from_true_labels(:,the_other_subjects);
    other_subjs_avg_sim_vec = mean(sim_vecs_from_the_other_subjects,2);
    other_subjs_avg_sim_vec_zscored = other_subjs_avg_sim_vec;
    % Generate the averaged similarity matrix for other subjects
    other_subjs_avg_sim_mat_zscored = squareform(other_subjs_avg_sim_vec);
    
    corrs_of_this_subjs_perms_with_other_subjs = zeros(1,num_perms);
    
    for perm_num = 1:num_perms,
        this_perm = all_possible_perms(perm_num,:);
        
        %%% Make the permed-label sim matrix
        sim_matrix_for_this_perm = this_subj_true_sim_mat_zscored(this_perm,this_perm);
        
        %%% Ok, now we have a sim_matrix for this perm
        vec_version_of_this_perms_sim_matrix = sim_matrix_for_this_perm(squareform_extractor_mat);
        this_corr = vec_version_of_this_perms_sim_matrix' * other_subjs_avg_sim_vec_zscored / ...
            (length_of_a_squareform_vec-1);
        
        corrs_of_this_subjs_perms_with_other_subjs(perm_num) = this_corr;
        
    end;  %%% End of loop through perms
    
    [across_subj_max_val,across_subj_max_ind] = ...
        max(corrs_of_this_subjs_perms_with_other_subjs);
    
    the_best_matching_perm = all_possible_perms(across_subj_max_ind,:);
    %%% Store this
    best_matching_perms_rec(:,subj_num) = the_best_matching_perm;
    %%% See how many perm entries were correct, and store it
    prop_correct = sum(the_best_matching_perm==[1:num_conds]) / num_conds;
    subjects_prop_correct_rec(subj_num) = prop_correct;
    classes_correct_rec(subj_num,:) = (the_best_matching_perm==[1:num_conds]);
    
    disp(['Subj ' num2str(subj_num) ': Best-matching perm with other subjects was ' ...
        num2str(the_best_matching_perm) ...
        ' and was r=' num2str(across_subj_max_val) ...
        '. Prop correct = ' num2str(prop_correct) ]);
    
    %%% If we didn't get 100% correct, let's see what the labels
    %%% of the swapped items were
    if prop_correct~=1,
        wrongly_labeled_items = find(the_best_matching_perm ~= [1:num_conds]);
        disp(['The wrongly labeled items were ' labels{wrongly_labeled_items} ]);
    end;
    
    Results{subj_num + 1,1} = num2str(subj_num);
    Results{subj_num + 1,2} = num2str(the_best_matching_perm);
    Results{subj_num + 1,3} = across_subj_max_val;
    Results{subj_num + 1,4} = prop_correct;
    
end;  %%% End of loop through subjs

mean_subj_prop_correct = mean(subjects_prop_correct_rec);
disp(['Average prop correct across all subjects = ' num2str(mean_subj_prop_correct) ]);
Results{8,1} = (['Averaged prop: ' num2str(mean_subj_prop_correct)]);

% save(fullfile('~/Documents/MATLAB/Results/Results_EMDAndDecoding/',FileName2),'Corr_All2','Results')

