function D = GDMtrxForloop(YNBP,XNBP,ZNBP) %#codegen
assert(isa(YNBP,'double'));
assert(isa(XNBP,'double'));
assert(isa(ZNBP,'double'));

D = zeros(YNBP*XNBP*ZNBP,YNBP*XNBP*ZNBP);
i= 1;
for y1=1:YNBP
    for x1=1:XNBP
        for nbo1=1:ZNBP
            
            j= 1;
            for y2=1:YNBP
                for x2=1:XNBP
                    for nbo2=1:ZNBP
                        D(i,j)= sqrt((y1-y2)^2 + (x1-x2)^2) + min( [abs(nbo1-nbo2) ZNBP-abs(nbo1-nbo2)] );
                        j=j+1;
                    end
                end
            end
            i= i+1;
            
        end
    end
end
end