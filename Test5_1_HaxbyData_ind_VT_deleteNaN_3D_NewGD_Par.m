clear all;
close all;

const_fac = 1;
extra_mass_penalty = -1;
FType = 3;

addpath('~/Documents/MATLAB/Code/FastEMD_PeleWerman');
addpath('~/Documents/MATLAB/Data/haxby2001/Mat_files/Activity maps')
addpath('~/Documents/MATLAB/Toolboxes/spm8/');
addpath('~/Documents/MATLAB/Toolboxes/NIfTI_20140122/');
addpath('~/Documents/MATLAB/Utilities/');

[conda,condb] = meshgrid(1:8,1:8);
conda = conda(:); condb = condb(:);
a = 1;
for ctr = 1:length(conda)
    if conda(ctr) > condb(ctr)
        cond_uni(a,1) = conda(ctr);
        cond_uni(a,2) = condb(ctr);
        a = a + 1;
    end
end

parpool;
for subid = 4
    tic;
    
    disp(['Constructing ground distance matrix for subject ' num2str(subid)]);
    load(['subj' num2str(subid) 'ind_VT.mat']);
    
    subj_coords = eval(['subj' num2str(subid) '_voxel_coords']) + 1;
    subj_coords = double(subj_coords);
    
    voxelnum = size(subj_coords,1);
    
    Xcoords = subj_coords(:,1);
    Ycoords = subj_coords(:,2);
    Zcoords = subj_coords(:,3);
    
    subj_data = zeros(1,voxelnum,8);
    for numcond = 1:8
        conddata = eval(['subj' num2str(subid) '_data{1,' num2str(numcond) '}']);
        conddata(isnan(conddata)) = 0;
        conddata = mean(conddata);
        subj_data(1,:,numcond) = conddata;
    end
    
    % Generate ground distance matrix
    % codegen -o GDMtrxForloop_coords_mex -report GDMtrxForloop_coords.m -args {Xcoords,Ycoords,Zcoords,voxelnum}
    % D = GDMtrxForloop_coords_mex(Xcoords,Ycoords,Zcoords,voxelnum);
    D = GDMtrxForloop_coords(Xcoords,Ycoords,Zcoords,voxelnum);
    %     figure;imagesc(D);
    
    thresh1 = round(quantile(D(:),0.95) + 1);
    D1 = min(D,thresh1);
    disp(['max Dist = ' num2str(max(D(:))) ', thresh = ' num2str(thresh1)]);
    %     figure;imagesc(D1);
    
    %     thresh1 = round(max(D(:)) * 0.75);
    %     D1 = min(D,thresh1);
    %     thresh2 = round(max(D(:)) * 0.5);
    %     D2 = min(D,thresh2);
    
    disp(['Calculating EMD for subject ' num2str(subid)]);
    ind_EMD = zeros(1,size(cond_uni,1));
    
    % Display parallel processing progress
    N = size(cond_uni,1);
    p = ProgressBar(N);
    parfor ctr = 1:N
        P = subj_data(1,:,cond_uni(ctr,1));
        Q = subj_data(1,:,cond_uni(ctr,2));
        
        %# normalize to [0,1]
        P1 = (P - min(P(:)))./(max(P(:)) - min(P(:)));
        Q1 = (Q - min(Q(:)))./(max(Q(:)) - min(Q(:)));
        
        % Fastest EMD (thresholded ground distance & metric)
        [dist,~] = emd_hat_gd_metric_mex(P1',Q1',D1,extra_mass_penalty,FType);
        
        ind_EMD(1,ctr) = dist;
        
        % Write results to .txt file
        % ----------------------------------------------
        fileID = fopen('Results_Subject4_norm2.txt','a');
        % ----------------------------------------------
        fprintf(fileID,'%12s %12s %12s\r\n','idx 1','idx 2','EMD dist');
        fprintf(fileID,'%12d %12d %12.4e\r\n',cond_uni(ctr,1),cond_uni(ctr,2),dist);
        p.progress;
    end
    p.stop;
    disp(['Subject' num2str(subid) ' is done, threshold value = '  num2str(thresh1)]);
    tEla = toc;
    
    FileName = ['EMDMtrx_ind_VT_VOI_3D_' num2str(thresh1) '_sub' num2str(subid) '_newGD.mat'];
    % save(fullfile('~/Documents/MATLAB/Results/Results_EMDAndDecoding',FileName),'ind_EMD','tEla');
    save('subject4_norm.mat','tEla','ind_EMD');
    
    sendmail('xixi.wang577@gmail.com', 'EMD Test', ['Subject ' num2str(subid) ' is done.']);
end

delete(gcp);
