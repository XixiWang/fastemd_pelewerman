clear all;
% clc;
close all;

im1 = zeros(3,3);
im1(1,1) = 1;
P = int32(im1(:));

im2 = zeros(3,3);
im2(1,3) = 1;
Q = int32(im2(:));

im3 = zeros(3,3);
im3(3,1) = 1;
X = int32(im3(:));

R = size(im1,1);
C = size(im1,2);

% Construct ground distance matrix
COST_MULT_FACTOR = 1000;
THRESHOLD = 5 * COST_MULT_FACTOR;
D = zeros(R*C,R*C,'int32');
j= 0;
for c1=1:C
    for r1=1:R
        j= j+1;
        i= 0;
        for c2=1:C
            for r2=1:R
                i= i+1;
                D(i,j)= min( [THRESHOLD (COST_MULT_FACTOR*sqrt((r1-r2)^2+(c1-c2)^2))] );
            end
        end
    end
end
extra_mass_penalty= int32(-1);
flowType= int32(3);

[dist,~] = emd_hat_gd_metric_mex(P,Q,D,extra_mass_penalty,flowType);
disp(dist);
[dist2,~] = emd_hat_gd_metric_mex(P,X,D,extra_mass_penalty,flowType);
disp(dist2);