% Perform EMD on 2D histograms
% Use single slice of the original 3D activity volume
clear all;

addpath('~/Documents/MATLAB/Code/FastEMD_PeleWerman/');
addpath('~/Documents/MATLAB/Data/haxby2001/Mat_files/Activity maps/');
addpath('~/Documents/MATLAB/Toolboxes/spm8/');
addpath('~/Documents/MATLAB/Toolboxes/NIfTI_20140122/');

const_fac = 1;
extra_mass_penalty = -1;
FType = 3;

% ----------------------------------
thresh = 3000;
% ----------------------------------

[conda,condb] = meshgrid(1:8,1:8);
conda = conda(:); condb = condb(:);
a = 1;
for ctr = 1:length(conda)
    if conda(ctr) > condb(ctr)
        cond_uni(a,1) = conda(ctr);
        cond_uni(a,2) = condb(ctr);
        a = a + 1;
    end
end

temp = load_nii('selected_voxels_vol.nii');
tempdata = temp.img;
[szx,szy,szz] = size(tempdata);

% Construct ground distance matrix
% Ground distance matrix D should be same for all subjects here
disp('Construct ground distance matrix D - transverse plane')
D_z = zeros(szx*szy,szx*szy);
j = 0;
for c1 = 1:szy
    for r1 = 1:szx
        j = j+1;
        i = 0;
        for c2 = 1:szy
            for r2 = 1:szx
                i = i+1;
                D_z(i,j) = min( [thresh (const_fac*sqrt((r1-r2)^2+(c1-c2)^2))] );
            end
        end
    end
end

EMDMtrx_All = zeros(6,size(cond_uni,1));

for subid = 1:6
    disp(['Calculating EMD for subject ' num2str(subid)]);
    
    for ctr = 1:size(cond_uni,1)
        tempP = load_nii(['selected_voxels_vol_subj' num2str(subid) '_cond' num2str(cond_uni(ctr,1)) '.nii']);
        tempQ = load_nii(['selected_voxels_vol_subj' num2str(subid) '_cond' num2str(cond_uni(ctr,2)) '.nii']);
        P = double(tempP.img(:,:,12));
        Q = double(tempQ.img(:,:,12));
        
        P = P(:);
        Q = Q(:);
        
        [dist,~] = emd_hat_gd_metric_mex(P,Q,D_z,extra_mass_penalty,FType);
        
        EMDMtrx_All(subid,ctr) = dist;
        clear tempP tempQ P Q
    end
    
    disp(['Subject ' num2str(subid) ' is done, threshold value = ' num2str(thresh)]);
end

FileName = ['EMDMtrx_SelVol_2D_' num2str(thresh) '_SingleSlice.mat'];
save(fullfile('~/Documents/MATLAB/Results/Results_EMDAndDecoding',FileName),'EMDMtrx_All');