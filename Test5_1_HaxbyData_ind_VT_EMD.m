% Perform EMD on 1D histograms
% Strach out 3D activity maps into 1D histogram

% UPDATE: when threshold = 3000, without deleting NaN values, dist =
% 3.23e07;

clear all;
close all;

addpath('~/Documents/MATLAB/Code/FastEMD_PeleWerman');
addpath('~/Documents/MATLAB/Data/haxby2001/Mat_files/Activity maps/VoxelsViaVTMasks_AllSub_AllConds/')
addpath('~/Documents/MATLAB/Toolboxes/spm8/');
addpath('~/Documents/MATLAB/Toolboxes/NIfTI_20140122/');

const_fac = 1;
extra_mass_penalty = -1;
FType = 3;

% ----------------------------------
thresh = 2000;
% ----------------------------------

[conda,condb] = meshgrid(1:8,1:8);
conda = conda(:); condb = condb(:);
a = 1;
for ctr = 1:length(conda)
    if conda(ctr) > condb(ctr)
        cond_uni(a,1) = conda(ctr);
        cond_uni(a,2) = condb(ctr);
        a = a + 1;
    end
end

EMDMtrx_All = zeros(6,size(cond_uni,1));
for subid = 1:6
    
    disp(['Calculating EMD for subject ' num2str(subid)]);
    
    % --------------------------------------------------
    load(['subj' num2str(subid) 'ind_VT.mat']);
    % --------------------------------------------------

    N = eval(['size(subj' num2str(subid) '_voxel_coords,1)']);
    disp(['Constructing ground distance matrix for subject ' num2str(subid)]);
    disp([num2str(N) ' voxels selected']);
    D= ones(N,N) .* thresh;
    for ctri = 1:N
        for ctrj = max([1 ctri - thresh + 1]):min([N ctri + thresh - 1])
            D(ctri,ctrj) = abs(ctri - ctrj);
        end
    end
    
    for ctr = 1:size(cond_uni,1)
        P = eval(['subj' num2str(subid) '_data{1,cond_uni(ctr,1)}']);
        Q = eval(['subj' num2str(subid) '_data{1,cond_uni(ctr,2)}']);
        
        % Delete zero elements
        P(isnan(P)) = 0;
        Q(isnan(Q)) = 0;
        
        P = double(mean(P));
        Q = double(mean(Q));
        
        % Fastest EMD (thresholded ground distance & metric)
        [dist,~] = emd_hat_gd_metric_mex(P',Q',D,extra_mass_penalty,FType);
        
        EMDMtrx_All(subid,ctr) = dist;
        clear tempP tempQ P Q
    end
    
    disp(['Subject' num2str(subid) ' is done, threshold value = '  num2str(thresh)]);
end

% --------------------------------------------------
FileName = ['EMDMtrx_ind_VT_1D_' num2str(thresh) '.mat'];
% --------------------------------------------------

save(fullfile('~/Documents/MATLAB/Results/Results_EMDAndDecoding',FileName),'EMDMtrx_All');