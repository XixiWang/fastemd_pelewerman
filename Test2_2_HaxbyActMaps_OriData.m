% Perform earth mover's distance (EMD) on 1D histograms
% All voxels
addpath(pwd);
addpath(fullfile(pwd,'Data_Haxby_ActMaps'));

% clear all;
% clc;
close all;

const_fac = 1;
extra_mass_penalty = -1;
FType = 3;

[conda,condb] = meshgrid(1:8,1:8);
conda = conda(:); condb = condb(:);
a = 1;
for ctr = 1:length(conda)
    if conda(ctr) > condb(ctr)
        cond_uni(a,1) = conda(ctr);
        cond_uni(a,2) = condb(ctr);
        a = a + 1;
    end
end

EMDMtrx_All = zeros(6,size(cond_uni,1));

for subid = 1:6
    load(['Averaged_fMRI_valuessubj' num2str(subid) '.mat']);
    
    DistMtrx2 = zeros(1,size(cond_uni,1));
    
    for condctr = 1:size(cond_uni,1)
        P = (averaged_fMRI_values(cond_uni(condctr,1),:))';
        Q = (averaged_fMRI_values(cond_uni(condctr,2),:))';
        
        % Apply a thresholded ground distance
        % Test different threshold values
        for ctr = 10
            thr = ctr * const_fac;
            
            % Generate ground distance mtrx
            D = ones(size(averaged_fMRI_values,2),size(averaged_fMRI_values,2));
            for ctri = 1:size(averaged_fMRI_values,2)
                for ctrj = max(1,ctri - thr + 1) : min(size(averaged_fMRI_values,2),ctri + thr - 1)
                    D(ctri,ctrj) = abs(ctri - ctrj);
                end
            end
            
            % Fastest EMD (thresholded ground distance & metric)
            [dist,~] = emd_hat_gd_metric_mex(P,Q,D,extra_mass_penalty,FType);
            % disp(['Threshold value = ' num2str(ctr) ', dist = ' num2str(dist)])
            
            DistMtrx2(1,condctr) = dist;
            % Thresholded ground distance only
            % [dist2,~]= emd_hat_mex(P,Q,D,extra_mass_penalty,FType);
            % disp(dist2)
            
            % Rubner EMD
            % sumBig=   max([sum(P(:)) sum(Q(:))]);
            % sumSmall= min([sum(P(:)) sum(Q(:))]);
            % D= double(D);
            % emd_rubner_mex_val= (sumSmall*emd_mex(P',Q',D)) + (sumBig-sumSmall)*max(D(:));
        end
    end
    
    disp(['Subject' num2str(subid) 'is done.'])
    
    EMDMtrx_All(subid,:) = DistMtrx2;
    clear DistMtrx2;
end

FileName = ['EMDMtrx_AllVol_thr_' num2str(thr) '.mat'];
save(FileName,'EMDMtrx_All');


