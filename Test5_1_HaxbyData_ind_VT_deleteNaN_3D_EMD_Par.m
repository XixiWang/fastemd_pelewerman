clear all;
close all;

const_fac = 1;
extra_mass_penalty = -1;
FType = 3;

addpath('~/Documents/MATLAB/Code/FastEMD_PeleWerman');
addpath('~/Documents/MATLAB/Data/haxby2001/Mat_files/Activity maps/VOIsViaVTMasks_AllSub_AllConds')
addpath('~/Documents/MATLAB/Toolboxes/spm8/');
addpath('~/Documents/MATLAB/Toolboxes/NIfTI_20140122/');

[conda,condb] = meshgrid(1:8,1:8);
conda = conda(:); condb = condb(:);
a = 1;
for ctr = 1:length(conda)
    if conda(ctr) > condb(ctr)
        cond_uni(a,1) = conda(ctr);
        cond_uni(a,2) = condb(ctr);
        a = a + 1;
    end
end


% for subid = 1:6
for subid = 1
    
    disp('Analyzing 3D VOIs for each condition')
    
    disp(['Constructing ground distance matrix for subject ' num2str(subid)]);
    data = load(['VTselected_voxels_VOI_subj' num2str(subid) '_allcond.mat']);
    tempdata = data.VOIs{1,1};
    [szx,szy,szz] = size(tempdata);
    
    tic;
    D = GDMtrxForloop_mex(szy,szx,szz);
    t1 = toc;
    %     figure;imagesc(D);
    
    thresh = round(max(D(:)) * 0.75);
    D1 = min(D,thresh);
    disp(['max Dist = ' num2str(max(D(:))) ', thresh = ' num2str(thresh)]);
    %     figure;imagesc(D1);
    
    disp(['Calculating EMD for subject ' num2str(subid)]);
    ind_EMD = zeros(1,size(cond_uni,1));
    
    tic;
    parpool(7);
    parfor ctr = 1:7
%     parfor ctr = 1:size(cond_uni,1)
        
        tempP = data.VOIs{1,cond_uni(ctr,1)};
        tempQ = data.VOIs{1,cond_uni(ctr,2)};
        
        P = tempP(:);
        Q = tempQ(:);
        
        % Fastest EMD (thresholded ground distance & metric)
        [dist,~] = emd_hat_gd_metric_mex(P,Q,D1,extra_mass_penalty,FType);
        
        ind_EMD(1,ctr) = dist;
    end
    delete(gcp);
    tEla = toc;
    
    disp(['Subject' num2str(subid) ' is done, threshold value = '  num2str(thresh)]);
    
    FileName = ['EMDMtrx_ind_VT_VOI_3D_' num2str(thresh) '_sub' num2str(subid) '.mat'];
    save(fullfile('~/Documents/MATLAB/Results/Results_EMDAndDecoding',FileName),'ind_EMD','tEla');
    
    clear D;
end