/*
 * GDMtrxForloop_coords_terminate.h
 *
 * Code generation for function 'GDMtrxForloop_coords_terminate'
 *
 * C source code generated on: Thu Dec 18 11:40:47 2014
 *
 */

#ifndef __GDMTRXFORLOOP_COORDS_TERMINATE_H__
#define __GDMTRXFORLOOP_COORDS_TERMINATE_H__
/* Include files */
#include <math.h>
#include <stdlib.h>
#include <string.h>
#include "mwmathutil.h"

#include "tmwtypes.h"
#include "mex.h"
#include "emlrt.h"
#include "blas.h"
#include "rtwtypes.h"
#include "GDMtrxForloop_coords_types.h"

/* Function Declarations */
extern void GDMtrxForloop_coords_atexit(emlrtStack *sp);
extern void GDMtrxForloop_coords_terminate(emlrtStack *sp);
#endif
/* End of code generation (GDMtrxForloop_coords_terminate.h) */
