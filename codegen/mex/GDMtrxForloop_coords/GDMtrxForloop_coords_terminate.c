/*
 * GDMtrxForloop_coords_terminate.c
 *
 * Code generation for function 'GDMtrxForloop_coords_terminate'
 *
 * C source code generated on: Thu Dec 18 11:40:47 2014
 *
 */

/* Include files */
#include "rt_nonfinite.h"
#include "GDMtrxForloop_coords.h"
#include "GDMtrxForloop_coords_terminate.h"

/* Function Definitions */
void GDMtrxForloop_coords_atexit(emlrtStack *sp)
{
  emlrtCreateRootTLS(&emlrtRootTLSGlobal, &emlrtContextGlobal, NULL, 1);
  sp->tls = emlrtRootTLSGlobal;
  emlrtEnterRtStackR2012b(sp);
  emlrtLeaveRtStackR2012b(sp);
  emlrtDestroyRootTLS(&emlrtRootTLSGlobal);
}

void GDMtrxForloop_coords_terminate(emlrtStack *sp)
{
  emlrtLeaveRtStackR2012b(sp);
  emlrtDestroyRootTLS(&emlrtRootTLSGlobal);
}

/* End of code generation (GDMtrxForloop_coords_terminate.c) */
