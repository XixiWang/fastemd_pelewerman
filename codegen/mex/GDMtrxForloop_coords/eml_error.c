/*
 * eml_error.c
 *
 * Code generation for function 'eml_error'
 *
 * C source code generated on: Thu Dec 18 11:40:47 2014
 *
 */

/* Include files */
#include "rt_nonfinite.h"
#include "GDMtrxForloop_coords.h"
#include "eml_error.h"

/* Variable Definitions */
static emlrtRTEInfo e_emlrtRTEI = { 20, 5, "eml_error",
  "/Applications/MATLAB_R2013b.app/toolbox/eml/lib/matlab/eml/eml_error.m" };

/* Function Definitions */
void eml_error(const emlrtStack *sp)
{
  static char_T cv0[4][1] = { { 's' }, { 'q' }, { 'r' }, { 't' } };

  emlrtErrorWithMessageIdR2012b(sp, &e_emlrtRTEI,
    "Coder:toolbox:ElFunDomainError", 3, 4, 4, cv0);
}

/* End of code generation (eml_error.c) */
