MATLAB="/Applications/MATLAB_R2013b.app"
Arch=maci64
ENTRYPOINT=mexFunction
MAPFILE=$ENTRYPOINT'.map'
PREFDIR="/axelUsers/xwang91/.matlab/R2013b"
OPTSFILE_NAME="./mexopts.sh"
. $OPTSFILE_NAME
COMPILER=$CC
. $OPTSFILE_NAME
echo "# Make settings for GDMtrxForloop_coords" > GDMtrxForloop_coords_mex.mki
echo "CC=$CC" >> GDMtrxForloop_coords_mex.mki
echo "CFLAGS=$CFLAGS" >> GDMtrxForloop_coords_mex.mki
echo "CLIBS=$CLIBS" >> GDMtrxForloop_coords_mex.mki
echo "COPTIMFLAGS=$COPTIMFLAGS" >> GDMtrxForloop_coords_mex.mki
echo "CDEBUGFLAGS=$CDEBUGFLAGS" >> GDMtrxForloop_coords_mex.mki
echo "CXX=$CXX" >> GDMtrxForloop_coords_mex.mki
echo "CXXFLAGS=$CXXFLAGS" >> GDMtrxForloop_coords_mex.mki
echo "CXXLIBS=$CXXLIBS" >> GDMtrxForloop_coords_mex.mki
echo "CXXOPTIMFLAGS=$CXXOPTIMFLAGS" >> GDMtrxForloop_coords_mex.mki
echo "CXXDEBUGFLAGS=$CXXDEBUGFLAGS" >> GDMtrxForloop_coords_mex.mki
echo "LD=$LD" >> GDMtrxForloop_coords_mex.mki
echo "LDFLAGS=$LDFLAGS" >> GDMtrxForloop_coords_mex.mki
echo "LDOPTIMFLAGS=$LDOPTIMFLAGS" >> GDMtrxForloop_coords_mex.mki
echo "LDDEBUGFLAGS=$LDDEBUGFLAGS" >> GDMtrxForloop_coords_mex.mki
echo "Arch=$Arch" >> GDMtrxForloop_coords_mex.mki
echo OMPFLAGS= >> GDMtrxForloop_coords_mex.mki
echo OMPLINKFLAGS= >> GDMtrxForloop_coords_mex.mki
echo "EMC_COMPILER=" >> GDMtrxForloop_coords_mex.mki
echo "EMC_CONFIG=optim" >> GDMtrxForloop_coords_mex.mki
"/Applications/MATLAB_R2013b.app/bin/maci64/gmake" -B -f GDMtrxForloop_coords_mex.mk
