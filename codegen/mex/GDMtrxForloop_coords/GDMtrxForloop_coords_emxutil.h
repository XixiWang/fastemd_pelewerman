/*
 * GDMtrxForloop_coords_emxutil.h
 *
 * Code generation for function 'GDMtrxForloop_coords_emxutil'
 *
 * C source code generated on: Thu Dec 18 11:40:47 2014
 *
 */

#ifndef __GDMTRXFORLOOP_COORDS_EMXUTIL_H__
#define __GDMTRXFORLOOP_COORDS_EMXUTIL_H__
/* Include files */
#include <math.h>
#include <stdlib.h>
#include <string.h>
#include "mwmathutil.h"

#include "tmwtypes.h"
#include "mex.h"
#include "emlrt.h"
#include "blas.h"
#include "rtwtypes.h"
#include "GDMtrxForloop_coords_types.h"

/* Function Declarations */
extern void emxEnsureCapacity(const emlrtStack *sp, emxArray__common *emxArray, int32_T oldNumel, int32_T elementSize, const emlrtRTEInfo *srcLocation);
extern void emxFree_real_T(emxArray_real_T **pEmxArray);
extern void emxInit_real_T(const emlrtStack *sp, emxArray_real_T **pEmxArray, int32_T numDimensions, const emlrtRTEInfo *srcLocation, boolean_T doPush);
#endif
/* End of code generation (GDMtrxForloop_coords_emxutil.h) */
