/*
 * GDMtrxForloop_coords_initialize.c
 *
 * Code generation for function 'GDMtrxForloop_coords_initialize'
 *
 * C source code generated on: Thu Dec 18 11:40:47 2014
 *
 */

/* Include files */
#include "rt_nonfinite.h"
#include "GDMtrxForloop_coords.h"
#include "GDMtrxForloop_coords_initialize.h"
#include "GDMtrxForloop_coords_data.h"

/* Function Definitions */
void GDMtrxForloop_coords_initialize(emlrtStack *sp, emlrtContext *aContext)
{
  emlrtBreakCheckR2012bFlagVar = emlrtGetBreakCheckFlagAddressR2012b();
  emlrtCreateRootTLS(&emlrtRootTLSGlobal, aContext, NULL, 1);
  sp->tls = emlrtRootTLSGlobal;
  emlrtClearAllocCountR2012b(sp, FALSE, 0U, 0);
  emlrtEnterRtStackR2012b(sp);
  emlrtFirstTimeR2012b(emlrtRootTLSGlobal);
}

/* End of code generation (GDMtrxForloop_coords_initialize.c) */
