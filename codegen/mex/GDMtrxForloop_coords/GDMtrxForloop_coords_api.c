/*
 * GDMtrxForloop_coords_api.c
 *
 * Code generation for function 'GDMtrxForloop_coords_api'
 *
 * C source code generated on: Thu Dec 18 11:40:47 2014
 *
 */

/* Include files */
#include "rt_nonfinite.h"
#include "GDMtrxForloop_coords.h"
#include "GDMtrxForloop_coords_api.h"
#include "GDMtrxForloop_coords_emxutil.h"

/* Variable Definitions */
static emlrtRTEInfo b_emlrtRTEI = { 1, 1, "GDMtrxForloop_coords_api", "" };

/* Function Declarations */
static real_T (*b_emlrt_marshallIn(const emlrtStack *sp, const mxArray *u, const
  emlrtMsgIdentifier *parentId))[27];
static real_T c_emlrt_marshallIn(const emlrtStack *sp, const mxArray *N, const
  char_T *identifier);
static real_T d_emlrt_marshallIn(const emlrtStack *sp, const mxArray *u, const
  emlrtMsgIdentifier *parentId);
static real_T (*e_emlrt_marshallIn(const emlrtStack *sp, const mxArray *src,
  const emlrtMsgIdentifier *msgId))[27];
static real_T (*emlrt_marshallIn(const emlrtStack *sp, const mxArray *Xcoords,
  const char_T *identifier))[27];
static const mxArray *emlrt_marshallOut(emxArray_real_T *u);
static real_T f_emlrt_marshallIn(const emlrtStack *sp, const mxArray *src, const
  emlrtMsgIdentifier *msgId);

/* Function Definitions */
static real_T (*b_emlrt_marshallIn(const emlrtStack *sp, const mxArray *u, const
  emlrtMsgIdentifier *parentId))[27]
{
  real_T (*y)[27];
  y = e_emlrt_marshallIn(sp, emlrtAlias(u), parentId);
  emlrtDestroyArray(&u);
  return y;
}
  static real_T c_emlrt_marshallIn(const emlrtStack *sp, const mxArray *N, const
  char_T *identifier)
{
  real_T y;
  emlrtMsgIdentifier thisId;
  thisId.fIdentifier = identifier;
  thisId.fParent = NULL;
  y = d_emlrt_marshallIn(sp, emlrtAlias(N), &thisId);
  emlrtDestroyArray(&N);
  return y;
}

static real_T d_emlrt_marshallIn(const emlrtStack *sp, const mxArray *u, const
  emlrtMsgIdentifier *parentId)
{
  real_T y;
  y = f_emlrt_marshallIn(sp, emlrtAlias(u), parentId);
  emlrtDestroyArray(&u);
  return y;
}

static real_T (*e_emlrt_marshallIn(const emlrtStack *sp, const mxArray *src,
  const emlrtMsgIdentifier *msgId))[27]
{
  real_T (*ret)[27];
  int32_T iv1[1];
  iv1[0] = 27;
  emlrtCheckBuiltInR2012b(sp, msgId, src, "double", FALSE, 1U, iv1);
  ret = (real_T (*)[27])mxGetData(src);
  emlrtDestroyArray(&src);
  return ret;
}
  static real_T (*emlrt_marshallIn(const emlrtStack *sp, const mxArray *Xcoords,
  const char_T *identifier))[27]
{
  real_T (*y)[27];
  emlrtMsgIdentifier thisId;
  thisId.fIdentifier = identifier;
  thisId.fParent = NULL;
  y = b_emlrt_marshallIn(sp, emlrtAlias(Xcoords), &thisId);
  emlrtDestroyArray(&Xcoords);
  return y;
}

static const mxArray *emlrt_marshallOut(emxArray_real_T *u)
{
  const mxArray *y;
  static const int32_T iv0[2] = { 0, 0 };

  const mxArray *m0;
  y = NULL;
  m0 = mxCreateNumericArray(2, (int32_T *)&iv0, mxDOUBLE_CLASS, mxREAL);
  mxSetData((mxArray *)m0, (void *)u->data);
  mxSetDimensions((mxArray *)m0, u->size, 2);
  emlrtAssign(&y, m0);
  return y;
}

static real_T f_emlrt_marshallIn(const emlrtStack *sp, const mxArray *src, const
  emlrtMsgIdentifier *msgId)
{
  real_T ret;
  emlrtCheckBuiltInR2012b(sp, msgId, src, "double", FALSE, 0U, 0);
  ret = *(real_T *)mxGetData(src);
  emlrtDestroyArray(&src);
  return ret;
}

void GDMtrxForloop_coords_api(emlrtStack *sp, const mxArray * const prhs[4],
  const mxArray *plhs[1])
{
  emxArray_real_T *D;
  real_T (*Xcoords)[27];
  real_T (*Ycoords)[27];
  real_T (*Zcoords)[27];
  real_T N;
  emlrtHeapReferenceStackEnterFcnR2012b(sp);
  emxInit_real_T(sp, &D, 2, &b_emlrtRTEI, TRUE);

  /* Marshall function inputs */
  Xcoords = emlrt_marshallIn(sp, emlrtAlias(prhs[0]), "Xcoords");
  Ycoords = emlrt_marshallIn(sp, emlrtAlias(prhs[1]), "Ycoords");
  Zcoords = emlrt_marshallIn(sp, emlrtAlias(prhs[2]), "Zcoords");
  N = c_emlrt_marshallIn(sp, emlrtAliasP(prhs[3]), "N");

  /* Invoke the target function */
  GDMtrxForloop_coords(sp, *Xcoords, *Ycoords, *Zcoords, N, D);

  /* Marshall function outputs */
  plhs[0] = emlrt_marshallOut(D);
  D->canFreeData = FALSE;
  emxFree_real_T(&D);
  emlrtHeapReferenceStackLeaveFcnR2012b(sp);
}

/* End of code generation (GDMtrxForloop_coords_api.c) */
