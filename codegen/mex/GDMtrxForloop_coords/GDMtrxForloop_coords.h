/*
 * GDMtrxForloop_coords.h
 *
 * Code generation for function 'GDMtrxForloop_coords'
 *
 * C source code generated on: Thu Dec 18 11:40:47 2014
 *
 */

#ifndef __GDMTRXFORLOOP_COORDS_H__
#define __GDMTRXFORLOOP_COORDS_H__
/* Include files */
#include <math.h>
#include <stdlib.h>
#include <string.h>
#include "mwmathutil.h"

#include "tmwtypes.h"
#include "mex.h"
#include "emlrt.h"
#include "blas.h"
#include "rtwtypes.h"
#include "GDMtrxForloop_coords_types.h"

/* Function Declarations */
extern void GDMtrxForloop_coords(const emlrtStack *sp, const real_T Xcoords[27], const real_T Ycoords[27], const real_T Zcoords[27], real_T N, emxArray_real_T *D);
#endif
/* End of code generation (GDMtrxForloop_coords.h) */
