/*
 * GDMtrxForloop_coords_data.h
 *
 * Code generation for function 'GDMtrxForloop_coords_data'
 *
 * C source code generated on: Thu Dec 18 11:40:47 2014
 *
 */

#ifndef __GDMTRXFORLOOP_COORDS_DATA_H__
#define __GDMTRXFORLOOP_COORDS_DATA_H__
/* Include files */
#include <math.h>
#include <stdlib.h>
#include <string.h>
#include "mwmathutil.h"

#include "tmwtypes.h"
#include "mex.h"
#include "emlrt.h"
#include "blas.h"
#include "rtwtypes.h"
#include "GDMtrxForloop_coords_types.h"

/* Variable Declarations */
extern const volatile char_T *emlrtBreakCheckR2012bFlagVar;
extern emlrtRSInfo d_emlrtRSI;
extern emlrtRSInfo e_emlrtRSI;
extern emlrtRSInfo f_emlrtRSI;
extern emlrtRSInfo g_emlrtRSI;
extern emlrtRSInfo h_emlrtRSI;
extern emlrtRSInfo i_emlrtRSI;
extern emlrtRSInfo j_emlrtRSI;
extern emlrtRSInfo k_emlrtRSI;
extern emlrtRSInfo l_emlrtRSI;
extern emlrtRSInfo m_emlrtRSI;
extern emlrtRSInfo n_emlrtRSI;
extern emlrtRSInfo o_emlrtRSI;
extern emlrtRSInfo q_emlrtRSI;
extern emlrtRSInfo r_emlrtRSI;
extern emlrtRSInfo s_emlrtRSI;
extern emlrtRSInfo t_emlrtRSI;
extern emlrtRSInfo u_emlrtRSI;
extern emlrtMCInfo emlrtMCI;
extern emlrtMCInfo b_emlrtMCI;
#endif
/* End of code generation (GDMtrxForloop_coords_data.h) */
