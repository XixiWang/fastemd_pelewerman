/*
 * GDMtrxForloop_coords.c
 *
 * Code generation for function 'GDMtrxForloop_coords'
 *
 * C source code generated on: Thu Dec 18 11:40:47 2014
 *
 */

/* Include files */
#include "rt_nonfinite.h"
#include "GDMtrxForloop_coords.h"
#include "GDMtrxForloop_coords_emxutil.h"
#include "eml_error.h"
#include "GDMtrxForloop_coords_data.h"

/* Variable Definitions */
static emlrtRSInfo emlrtRSI = { 7, "GDMtrxForloop_coords",
  "/axelUsers/xwang91/Documents/MATLAB/Code/FastEMD_PeleWerman/GDMtrxForloop_coords.m"
};

static emlrtRSInfo b_emlrtRSI = { 11, "GDMtrxForloop_coords",
  "/axelUsers/xwang91/Documents/MATLAB/Code/FastEMD_PeleWerman/GDMtrxForloop_coords.m"
};

static emlrtRSInfo c_emlrtRSI = { 12, "GDMtrxForloop_coords",
  "/axelUsers/xwang91/Documents/MATLAB/Code/FastEMD_PeleWerman/GDMtrxForloop_coords.m"
};

static emlrtRSInfo p_emlrtRSI = { 14, "sqrt",
  "/Applications/MATLAB_R2013b.app/toolbox/eml/lib/matlab/elfun/sqrt.m" };

static emlrtRTEInfo emlrtRTEI = { 1, 14, "GDMtrxForloop_coords",
  "/axelUsers/xwang91/Documents/MATLAB/Code/FastEMD_PeleWerman/GDMtrxForloop_coords.m"
};

static emlrtRTEInfo c_emlrtRTEI = { 9, 5, "GDMtrxForloop_coords",
  "/axelUsers/xwang91/Documents/MATLAB/Code/FastEMD_PeleWerman/GDMtrxForloop_coords.m"
};

static emlrtRTEInfo d_emlrtRTEI = { 10, 9, "GDMtrxForloop_coords",
  "/axelUsers/xwang91/Documents/MATLAB/Code/FastEMD_PeleWerman/GDMtrxForloop_coords.m"
};

static emlrtBCInfo emlrtBCI = { 1, 27, 12, 25, "Zcoords", "GDMtrxForloop_coords",
  "/axelUsers/xwang91/Documents/MATLAB/Code/FastEMD_PeleWerman/GDMtrxForloop_coords.m",
  0 };

static emlrtBCInfo b_emlrtBCI = { 1, 27, 12, 41, "Zcoords",
  "GDMtrxForloop_coords",
  "/axelUsers/xwang91/Documents/MATLAB/Code/FastEMD_PeleWerman/GDMtrxForloop_coords.m",
  0 };

static emlrtBCInfo c_emlrtBCI = { 1, 27, 12, 69, "Zcoords",
  "GDMtrxForloop_coords",
  "/axelUsers/xwang91/Documents/MATLAB/Code/FastEMD_PeleWerman/GDMtrxForloop_coords.m",
  0 };

static emlrtBCInfo d_emlrtBCI = { 1, 27, 12, 85, "Zcoords",
  "GDMtrxForloop_coords",
  "/axelUsers/xwang91/Documents/MATLAB/Code/FastEMD_PeleWerman/GDMtrxForloop_coords.m",
  0 };

static emlrtDCInfo emlrtDCI = { 8, 11, "GDMtrxForloop_coords",
  "/axelUsers/xwang91/Documents/MATLAB/Code/FastEMD_PeleWerman/GDMtrxForloop_coords.m",
  1 };

static emlrtDCInfo b_emlrtDCI = { 8, 11, "GDMtrxForloop_coords",
  "/axelUsers/xwang91/Documents/MATLAB/Code/FastEMD_PeleWerman/GDMtrxForloop_coords.m",
  4 };

static emlrtDCInfo c_emlrtDCI = { 8, 13, "GDMtrxForloop_coords",
  "/axelUsers/xwang91/Documents/MATLAB/Code/FastEMD_PeleWerman/GDMtrxForloop_coords.m",
  1 };

static emlrtDCInfo d_emlrtDCI = { 8, 13, "GDMtrxForloop_coords",
  "/axelUsers/xwang91/Documents/MATLAB/Code/FastEMD_PeleWerman/GDMtrxForloop_coords.m",
  4 };

static emlrtBCInfo e_emlrtBCI = { 1, 27, 11, 35, "Ycoords",
  "GDMtrxForloop_coords",
  "/axelUsers/xwang91/Documents/MATLAB/Code/FastEMD_PeleWerman/GDMtrxForloop_coords.m",
  0 };

static emlrtBCInfo f_emlrtBCI = { 1, 27, 11, 51, "Ycoords",
  "GDMtrxForloop_coords",
  "/axelUsers/xwang91/Documents/MATLAB/Code/FastEMD_PeleWerman/GDMtrxForloop_coords.m",
  0 };

static emlrtBCInfo g_emlrtBCI = { 1, 27, 11, 71, "Xcoords",
  "GDMtrxForloop_coords",
  "/axelUsers/xwang91/Documents/MATLAB/Code/FastEMD_PeleWerman/GDMtrxForloop_coords.m",
  0 };

static emlrtBCInfo h_emlrtBCI = { 1, 27, 11, 87, "Xcoords",
  "GDMtrxForloop_coords",
  "/axelUsers/xwang91/Documents/MATLAB/Code/FastEMD_PeleWerman/GDMtrxForloop_coords.m",
  0 };

static emlrtBCInfo i_emlrtBCI = { -1, -1, 11, 15, "D", "GDMtrxForloop_coords",
  "/axelUsers/xwang91/Documents/MATLAB/Code/FastEMD_PeleWerman/GDMtrxForloop_coords.m",
  0 };

static emlrtBCInfo j_emlrtBCI = { -1, -1, 11, 20, "D", "GDMtrxForloop_coords",
  "/axelUsers/xwang91/Documents/MATLAB/Code/FastEMD_PeleWerman/GDMtrxForloop_coords.m",
  0 };

/* Function Definitions */
void GDMtrxForloop_coords(const emlrtStack *sp, const real_T Xcoords[27], const
  real_T Ycoords[27], const real_T Zcoords[27], real_T N, emxArray_real_T *D)
{
  int32_T ixstart;
  real_T mtmp;
  int32_T ix;
  boolean_T exitg1;
  int32_T i0;
  real_T a;
  real_T b_a;
  int32_T i1;
  emlrtStack st;
  emlrtStack b_st;
  st.prev = sp;
  st.tls = sp->tls;
  b_st.prev = &st;
  b_st.tls = st.tls;
  st.site = &emlrtRSI;
  ixstart = 1;
  mtmp = Zcoords[0];
  if (muDoubleScalarIsNaN(Zcoords[0])) {
    ix = 2;
    exitg1 = FALSE;
    while ((exitg1 == FALSE) && (ix < 28)) {
      ixstart = ix;
      if (!muDoubleScalarIsNaN(Zcoords[ix - 1])) {
        mtmp = Zcoords[ix - 1];
        exitg1 = TRUE;
      } else {
        ix++;
      }
    }
  }

  if (ixstart < 27) {
    while (ixstart + 1 < 28) {
      if (Zcoords[ixstart] > mtmp) {
        mtmp = Zcoords[ixstart];
      }

      ixstart++;
    }
  }

  i0 = D->size[0] * D->size[1];
  a = emlrtNonNegativeCheckFastR2012b(N, &b_emlrtDCI, sp);
  D->size[0] = (int32_T)emlrtIntegerCheckFastR2012b(a, &emlrtDCI, sp);
  a = emlrtNonNegativeCheckFastR2012b(N, &d_emlrtDCI, sp);
  D->size[1] = (int32_T)emlrtIntegerCheckFastR2012b(a, &c_emlrtDCI, sp);
  emxEnsureCapacity(sp, (emxArray__common *)D, i0, (int32_T)sizeof(real_T),
                    &emlrtRTEI);
  a = emlrtNonNegativeCheckFastR2012b(N, &b_emlrtDCI, sp);
  b_a = emlrtNonNegativeCheckFastR2012b(N, &d_emlrtDCI, sp);
  ixstart = (int32_T)emlrtIntegerCheckFastR2012b(a, &emlrtDCI, sp) * (int32_T)
    emlrtIntegerCheckFastR2012b(b_a, &c_emlrtDCI, sp);
  for (i0 = 0; i0 < ixstart; i0++) {
    D->data[i0] = 0.0;
  }

  emlrtForLoopVectorCheckR2012b(1.0, 1.0, N, mxDOUBLE_CLASS, (int32_T)N,
    &c_emlrtRTEI, sp);
  ixstart = 0;
  while (ixstart <= (int32_T)N - 1) {
    emlrtForLoopVectorCheckR2012b(1.0, 1.0, N, mxDOUBLE_CLASS, (int32_T)N,
      &d_emlrtRTEI, sp);
    ix = 0;
    while (ix <= (int32_T)N - 1) {
      st.site = &c_emlrtRSI;
      i0 = ixstart + 1;
      emlrtDynamicBoundsCheckFastR2012b(i0, 1, 27, &emlrtBCI, &st);
      i0 = ix + 1;
      emlrtDynamicBoundsCheckFastR2012b(i0, 1, 27, &b_emlrtBCI, &st);
      st.site = &c_emlrtRSI;
      i0 = ixstart + 1;
      emlrtDynamicBoundsCheckFastR2012b(i0, 1, 27, &c_emlrtBCI, &st);
      i0 = ix + 1;
      emlrtDynamicBoundsCheckFastR2012b(i0, 1, 27, &d_emlrtBCI, &st);
      st.site = &b_emlrtRSI;
      a = Ycoords[emlrtDynamicBoundsCheckFastR2012b(ixstart + 1, 1, 27,
        &e_emlrtBCI, &st) - 1] - Ycoords[emlrtDynamicBoundsCheckFastR2012b(ix +
        1, 1, 27, &f_emlrtBCI, &st) - 1];
      st.site = &b_emlrtRSI;
      b_a = Xcoords[emlrtDynamicBoundsCheckFastR2012b(ixstart + 1, 1, 27,
        &g_emlrtBCI, &st) - 1] - Xcoords[emlrtDynamicBoundsCheckFastR2012b(ix +
        1, 1, 27, &h_emlrtBCI, &st) - 1];
      st.site = &b_emlrtRSI;
      a = a * a + b_a * b_a;
      if (a < 0.0) {
        b_st.site = &p_emlrtRSI;
        eml_error(&b_st);
      }

      st.site = &c_emlrtRSI;
      i0 = D->size[0];
      i1 = D->size[1];
      D->data[(emlrtDynamicBoundsCheckFastR2012b(ixstart + 1, 1, i0, &i_emlrtBCI,
                sp) + D->size[0] * (emlrtDynamicBoundsCheckFastR2012b(ix + 1, 1,
                 i1, &j_emlrtBCI, sp) - 1)) - 1] = muDoubleScalarSqrt(a) +
        muDoubleScalarMin(muDoubleScalarAbs(Zcoords[ixstart] - Zcoords[ix]),
                          mtmp - muDoubleScalarAbs(Zcoords[ixstart] - Zcoords[ix]));
      ix++;
      emlrtBreakCheckFastR2012b(emlrtBreakCheckR2012bFlagVar, sp);
    }

    ixstart++;
    emlrtBreakCheckFastR2012b(emlrtBreakCheckR2012bFlagVar, sp);
  }
}

/* End of code generation (GDMtrxForloop_coords.c) */
