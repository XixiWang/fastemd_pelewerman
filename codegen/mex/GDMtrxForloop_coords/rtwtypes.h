/*
 * rtwtypes.h
 *
 * Code generation for function 'GDMtrxForloop_coords'
 *
 * C source code generated on: Thu Dec 18 11:40:47 2014
 *
 */

#ifndef __RTWTYPES_H__
#define __RTWTYPES_H__
#include "tmwtypes.h"
/* 
 * TRUE/FALSE definitions
 */
#ifndef TRUE
#define TRUE (1U)
#endif 
#ifndef FALSE
#define FALSE (0U)
#endif 
#endif
/* End of code generation (rtwtypes.h) */
