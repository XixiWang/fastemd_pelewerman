/*
 * _coder_GDMtrxForloop_coords_info.h
 *
 * Code generation for function 'GDMtrxForloop_coords'
 *
 * C source code generated on: Thu Dec 18 11:40:47 2014
 *
 */

#ifndef ___CODER_GDMTRXFORLOOP_COORDS_INFO_H__
#define ___CODER_GDMTRXFORLOOP_COORDS_INFO_H__
/* Include files */
#include "tmwtypes.h"
#include "mex.h"
#include "emlrt.h"


/* Function Declarations */
extern const mxArray *emlrtMexFcnResolvedFunctionsInfo(void);

#endif
/* End of code generation (_coder_GDMtrxForloop_coords_info.h) */
