clear all;
close all;

addpath('~/Documents/MATLAB/Data/haxby2001/Mat_files/Activity maps');
% Load activity maps generated from individual VT masks

addpath('~/Documents/MATLAB/Code/FastEMD_PeleWerman');
addpath('~/Documents/MATLAB/Data/haxby2001/Mat_files/Activity maps/VoxelsViaVTMasks_AllSub_AllConds/')
addpath('~/Documents/MATLAB/Toolboxes/spm8/');
addpath('~/Documents/MATLAB/Toolboxes/NIfTI_20140122/');

[conda,condb] = meshgrid(1:8,1:8);
conda = conda(:); condb = condb(:);
a = 1;
for ctr = 1:length(conda)
    if conda(ctr) > condb(ctr)
        cond_uni(a,1) = conda(ctr);
        cond_uni(a,2) = condb(ctr);
        a = a + 1;
    end
end

EMDMtrx_All = zeros(6,size(cond_uni,1));
tic;
for subid = 1:6
    
    disp(['Calculating EMD for subject ' num2str(subid)]);
    disp(['Analyze 2D images from 3D VOIs'])
    % --------------------------------------------------
    load(['subj' num2str(subid) 'ind_VT.mat']);
    % --------------------------------------------------
    
    tempdata = eval(['subj' num2str(subid) '_data{1,1}']);
    tempcoords = eval(['subj' num2str(subid) '_voxel_coords']);
    [szx,szy] = size(tempdata);
    tempdata = mean(tempdata);
    
    nanidx = isnan(tempdata);
    nan_num = length(nanidx);
    N = length(tempdata(~isnan(tempdata)));

    data_coords = tempcoords(~nanidx,:) + 1;
    
    disp(['Constructing ground distance matrix for subject ' num2str(subid)]);
    disp([num2str(N) ' voxels selected']);
    
    % Construct ground distance matrix for the original 3D image
    D= ones(N,N) .* thresh;
    for ctri = 1:N
        for ctrj = max([1 ctri - thresh + 1]):min([N ctri + thresh - 1])
            D(ctri,ctrj) = abs(ctri - ctrj);
        end
    end
    
    ind_EMD = zeros(1,size(cond_uni,1));
    
    act_data = zeros(szx,szy,8);
    
    for ctr = 1:8
        act_data(:,:,ctr) = eval(['subj' num2str(subid) '_data{1,ctr}']);
    end
    
%     matlabpool open;
    for ctr = 1:size(cond_uni,1)
        P = act_data(:,:,cond_uni(ctr,1));
        Q = act_data(:,:,cond_uni(ctr,2));
        
        % Delete zero elements
        P = double(mean(P));
        Q = double(mean(Q));
        
        P(isnan(P)) = [];
        Q(isnan(Q)) = [];
        
        % Fastest EMD (thresholded ground distance & metric)
        [dist,~] = emd_hat_gd_metric_mex(P',Q',D,extra_mass_penalty,FType);
        
        ind_EMD(1,ctr) = dist;
        EMDMtrx_All(subid,ctr) = dist;
    end
%     matlabpool close;
    
    disp(['Subject' num2str(subid) ' is done, threshold value = '  num2str(thresh)]);
    
    FileName = ['EMDMtrx_ind_VT_1D_' num2str(thresh) '_sub' num2str(subid) '.mat'];
    save(FileName,'ind_EMD');
end