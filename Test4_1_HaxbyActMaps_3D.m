% Perform earth mover's distance (EMD) on 3D histograms
% Assess voxels that are selected based on t-maps and F-maps
% The original data size:
% % % % % % 3D histograms???? OUT OF MEMORY!!

addpath('~/Documents/MATLAB/Code/FastEMD_PeleWerman');
addpath('~/Documents/MATLAB/Data/haxby2001/Mat_files/Activity maps/')
addpath('~/Documents/MATLAB/Toolboxes/spm8/');
addpath('~/Documents/MATLAB/Toolboxes/NIfTI_20140122/');

clear all;
% clc;
close all;

const_fac = 1;
extra_mass_penalty = -1;
FType = 3;
thresh = 2;

[conda,condb] = meshgrid(1:8,1:8);
conda = conda(:); condb = condb(:);
a = 1;
for ctr = 1:length(conda)
    if conda(ctr) > condb(ctr)
        cond_uni(a,1) = conda(ctr);
        cond_uni(a,2) = condb(ctr);
        a = a + 1;
    end
end

% Generate the ground distance matrix (ground distance is same for each
% subject
temp = load_nii(['selected_voxels_vol.nii']);
tempdata = temp.img;

szx = size(tempdata,1);
szy = size(tempdata,2);
szz = size(tempdata,3);
N = szx*szy*szz;

disp('Construct ground distance matrix for all subjects');
tic;
D = zeros(N,N);
i = 1;
for y1 = 1:szy
    for x1 = 1:szx
        for nbo1 = 1:szz
            j = 1;
            for y2 = 1:szy
                for x2 = 1:szx
                    for nbo2 = 1:szz
                        D(i,j) = (sqrt((y1-y2)^2 + (x1-x2)^2) + ...
                            min( [abs(nbo1-nbo2) szz-abs(nbo1-nbo2)] ));
                        j = j+1;
                    end
                end
            end 
            i = i+1;
        end
    end
end
maxDist = max(D(:));
D = min(D,thresh);
tgd = toc;

EMDMtrx_All = zeros(6,size(cond_uni,1));
for subid = 1:6
   
    disp(['Calculating EMD for subject ' num2str(subid)]);
    DistMtrx = zeros(1,size(cond_uni,1));

    for ctr = 1:size(cond_uni,1)
        tempP = load_nii(['selected_voxels_vol_subj' num2str(subid) '_cond' num2str(cond_uni(ctr,1)) '.nii']);
        tempQ = load_nii(['selected_voxels_vol_subj' num2str(subid) '_cond' num2str(cond_uni(ctr,2)) '.nii']);
        P = tempP.img(:);
        Q = tempQ.img(:);
        
        % Fastest EMD (thresholded ground distance & metric)
        [dist,~] = emd_hat_gd_metric_mex(P,Q,D,extra_mass_penalty,FType);
        
        DistMtrx(1,ctr) = dist;
        clear tempP tempQ P Q
    end
    
    disp(['Subject' num2str(subid) ' is done, threshold value = '  num2str(thresh)]);
    EMDMtrx_All(subid,:) = DistMtrx;
end


